#!/usr/bin/env bash
PID=
if [ -f './pid' ]; then
    PID=$(cat ./pid)
fi

if ps -p $PID > /dev/null
then
   echo "Service with $PID is already running"
else
    nohup /usr/lib/jvm/jre-1.8.0-openjdk/bin/java -jar  /home/piepadmin/piepapijava/piep.jar > piepapi.log 2> piepapi.log < /dev/null & PID=$!; echo $PID > ./pid
fi

