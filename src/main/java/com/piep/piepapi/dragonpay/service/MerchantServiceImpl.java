package com.piep.piepapi.dragonpay.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.piep.piepapi.configuration.Config;
import com.piep.piepapi.configuration.SOAPConnector;
import com.piep.piepapi.schemas.dragonpay.GetTxnToken;
import com.piep.piepapi.schemas.dragonpay.GetTxnTokenResponse;

public class MerchantServiceImpl implements MerchantService {

	@Autowired
	SOAPConnector soapConnector;
	
	@Autowired
	Config config;

	@Override
	public GetTxnTokenResponse getTransactionToken(GetTxnToken getTransactionTokenRequest) {

		GetTxnTokenResponse response = (GetTxnTokenResponse) soapConnector
				.callWebService(config.getDragonpayBaseUrl(), getTransactionTokenRequest);
		return response;
	}

}
