package com.piep.piepapi.dragonpay.service;

import com.piep.piepapi.schemas.dragonpay.GetTxnToken;
import com.piep.piepapi.schemas.dragonpay.GetTxnTokenResponse;

public interface MerchantService {

	GetTxnTokenResponse getTransactionToken(GetTxnToken getTransactionTokenRequest);
}
