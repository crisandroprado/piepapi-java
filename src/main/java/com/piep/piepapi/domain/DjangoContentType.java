package com.piep.piepapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Table(name = "django_content_type")
@Data
public class DjangoContentType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "app_label", length = 128)
	@Size(max = 128)
	private String appLabel;
	
	@Column(name = "model", length = 128)
	@Size(max = 128)
	private String model;
	
	
 	
}
