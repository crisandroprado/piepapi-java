package com.piep.piepapi.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "events_invitation")
@Data
public class EventsInvitation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "addresse_full_name", length = 255)
	@Size(max = 128)
	private String addresseFullName;

	@Column(name = "addresse_designation", length = 255)
	@Size(max = 128)
	private String addresseDesignation;

	@Column(name = "addresse_agency", length = 255)
	@Size(max = 128)
	private String addresseAgency;

	@Column(name = "addresse_attention", length = 255)
	@Size(max = 128)
	private String addresseAttention;

	@Column(name = "requesting_fullname", length = 255)
	@Size(max = 128)
	private String requestingFullName;

	@Column(name = "requesting_prc_number", length = 255)
	@Size(max = 128)
	private String requestingPrcNumber;

	@Column(name = "requesting_email_address", length = 255)
	@Size(max = 128)
	private String requestingEmailAddress;
	
	@Column(name = "user_id")
	private Integer userId;
	
	@Column(name = "event_id")
	private Integer eventId;
	
	@Transient
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
	private User user;
	
	@Transient
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "event_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Event event;


}
