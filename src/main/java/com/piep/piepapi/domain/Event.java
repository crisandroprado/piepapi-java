package com.piep.piepapi.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "events_event")
@Data
public class Event {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "title", length = 100)
	@Size(max = 100)
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "location", length = 255)
	@Size(max = 255)
	private String location;

	@Column(name = "fee")
	private int fee;

	@Column(name = "website", length = 255)
	@Size(max = 255)
	private String website;

	@Column(name = "banner", length = 100)
	@Size(max = 100)
	private String banner;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "user_id")
	private Integer userId;

	@Column(name = "chapter_id")
	private Integer chapterId;

	@Column(name = "early_bird_end_date")
	private Date earlyBirdEndDate;

	@Column(name = "early_bird_fee")
	private Integer earlyBirdFee;

	@Column(name = "early_bird_start_date")
	private Date earlyBirdStartDate;

	@Column(name = "members_only")
	@Range(min = 0, max = 1)
	private int membersOnly = 0;

	@Column(name = "hide_amount")
	@Range(min = 0, max = 1)
	private int hideAmount = 0;

	@Column(name = "created_by_president")
	@Range(min = 0, max = 1)
	private int createdByPresident = 0;
	
	@Column(name = "social_media_banner", length = 100)
	@Size(max = 100)
	private String socialMediaBanner;
	
	@Transient
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
	private User user;
	
	@Transient
	@ManyToOne
	@JoinColumn(name = "chapter_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Chapter chapter;
	
	/*@Transient
	private String duration;

	public String getDuration() {
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy");
		if (startDate == endDate)
			duration =String.format("%s",sdf.format(startDate));
		else if(endDate!=null)
			duration =String.format("%s - %s",sdf.format(startDate),sdf.format(endDate));
		else
			duration="";
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}*/
	
	
	/*@Transient
	@JsonIgnore
	private int amount;

	public int getAmount() {
		Date today = new Date();
        amount = fee;

        if (earlyBirdEndDate != null || earlyBirdEndDate !=null)
            return amount;

        if(today.after(earlyBirdEndDate)   && today.before(earlyBirdEndDate))
            amount = earlyBirdFee;

        return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}*/
	

}
