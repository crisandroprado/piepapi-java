package com.piep.piepapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Table(name = "users_user")
@Data
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "first_name", length = 30)
	@Size(max = 30)
	private String firstName;

	@Column(name = "last_name", length = 150)
	@Size(max = 150)
	private String lastName;

	@Column(name = "email", length = 254)
	@Size(max = 254)
	private String email;

	@Column(name = "prc_license_number", length = 50)
	@Size(max = 50)
	private String prcLicenseNumber;

	@Column(name = "middle_name", length = 255)
	@Size(max = 255)
	private String middleName;

	@Column(name = "id_picture", length = 100)
	@Size(max = 100)
	private String idPicture;

	@Column(name = "rfid", length = 50)
	@Size(max = 50)
	private String rfid;
	
	@Column(name = "chapter_id")
	private Integer chapterId;
	
	@Column(name = "skills")
	private String skills;
	
	@Transient
	private String fullname;

	public String getFullname() {
		fullname = String.format("%s %s %s", firstName, middleName,
					lastName);
		if (fullname.equals(""))
			fullname = email;
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	/*
	 * @OneToOne
	 * 
	 * @JoinColumn(name = "chapter_id", referencedColumnName = "id", insertable =
	 * false, updatable = false) private Chapter chapter;
	 * 
	 * @Transient
	 * 
	 * @OneToOne
	 * 
	 * @JoinColumn(name = "id", referencedColumnName = "user_id", insertable =
	 * false, updatable = false) private UserGroup userGroup;
	 */

}
