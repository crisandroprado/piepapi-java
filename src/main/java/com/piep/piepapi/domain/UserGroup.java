package com.piep.piepapi.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "users_user_groups")
@Data
public class UserGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "user_id")
	private int userId;
	
	@Column(name = "group_id")
	private int groupId;
	
	@Transient
	@OneToMany
	@JoinColumn(name = "group_id", referencedColumnName = "id", insertable = false, updatable = false)
	private List<AuthGroup> groups = new ArrayList<AuthGroup>();
 	
}
