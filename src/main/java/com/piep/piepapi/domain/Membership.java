package com.piep.piepapi.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "payments_membership")
@Data
public class Membership {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "year")
	private int year;

	@Column(name = "payment_id")
	private Integer paymentId;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "created_date")
	private Date createdDate;
	
	@Transient
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
	private User user;
	
	@Transient
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "payment_id", referencedColumnName = "id", insertable = false, updatable = false)
	private PaymentHeader payment;


}
