package com.piep.piepapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "auth_permission")
@Data
public class AuthPermission {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "name", length = 128)
	@Size(max = 128)
	private String name;
	
	@Column(name = "content_type_id")
	private int content_type_id;
	
	@Column(name = "codename", length = 128)
	@Size(max = 128)
	private String codeName;
	
	@Transient
	@ManyToOne
	@JoinColumn(name = "content_type_id", referencedColumnName = "id", insertable = false, updatable = false)
	private DjangoContentType contentType;
	
	
 	
}
