package com.piep.piepapi.domain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "user_users_attendance")
@Data
public class UserAttendance {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "rfid", length = 50)
	@Size(max = 50)
	private String rfid;

	@Column(name = "date_in", length = 50)
	@Size(max = 50)
	private String dateIn;

	@Column(name = "time_in", length = 50)
	@Size(max = 50)
	private String timeIn;

	@Column(name = "date_out", length = 50)
	@Size(max = 50)
	private String dateOut;

	@Column(name = "time_out", length = 50)
	@Size(max = 50)
	private String timeOut;

	@Column(name = "count")
	private Integer count;
	
	@Column(name = "event_id")
	private Integer eventId;
	
	@Transient
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "rfid", referencedColumnName = "rfid", insertable = false, updatable = false)
	private User user;
	
	@Transient
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "event_id", referencedColumnName = "id", insertable = false, updatable = false)
	private MemberEvent event;
	
	@Transient
	private Long totalMinutes;

	public Long getTotalMinutes() {
		try {
			String dateTimeOut = String.format("%s %s", dateOut, timeOut);
			Date endDate = new SimpleDateFormat("MMM-dd-yyyy hh:mm:ss:SSSS").parse(dateTimeOut);  
			String dateTimeIn = String.format("%s %s", dateIn, timeIn);
			Date startDate = new SimpleDateFormat("MMM-dd-yyyy hh:mm:ss:SSSS").parse(dateTimeIn);  
			long end = endDate.getTime();
			long start = startDate.getTime();
			long diff = end - start;
			totalMinutes = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
		}catch(Exception ex) {
			totalMinutes = 0L;
		}
		return totalMinutes;
	}

	public void setTotalMinutes(Long totalMinutes) {
		this.totalMinutes = totalMinutes;
	}
	
	
}