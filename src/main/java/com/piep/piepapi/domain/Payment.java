package com.piep.piepapi.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "payments_payment")
@Data
public class Payment {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "type", length = 50)
	@Size(max = 50)
	private String type;

	@Column(name = "message")
	private String message;

	@Column(name = "method", length = 60)
	@Size(max = 60)
	private String method;

	@Column(name = "status", length = 1)
	@Size(max = 1)
	private String status;

	@Column(name = "currency", length = 3)
	@Size(max = 3)
	private String currency;

	@Column(name = "transaction_number", length = 200)
	@Size(max = 200)
	private String transactionNumber;

	@Column(name = "reference_number", length = 100)
	@Size(max = 100)
	private String referenceNumber;

	@Column(name = "description", length = 255)
	@Size(max = 255)
	private String description;

	@Column(name = "token", length = 200)
	@Size(max = 200)
	private String token;

	@Column(name = "param1", length = 80)
	@Size(max = 80)
	private String param1;

	@Column(name = "param2", length = 80)
	@Size(max = 80)
	private String param2;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "user_id")
	private Integer userId;

	@Column(name = "digest", length = 255)
	@Size(max = 255)
	private String digest;

	@Column(name = "proc_id", length = 255)
	@Size(max = 255)
	private String procId;

	@Transient
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
	private User user;

	/*
	 * @Transient private Boolean isPaid;
	 * 
	 * public Boolean getIsPaid() { return status ==
	 * PaymentStatus.success.getValue(); }
	 * 
	 * public void setIsPaid(Boolean isPaid) { this.isPaid = isPaid; }
	 * 
	 * @Transient
	 * 
	 * @JsonIgnore private String updateDate;
	 * 
	 * public String getUpdateDate() { SimpleDateFormat sdf = new
	 * SimpleDateFormat("MMM dd yyyy"); updateDate
	 * =String.format("%s",sdf.format(updatedAt)); return updateDate; }
	 * 
	 * public void setUpdateDate(String updateDate) { this.updateDate = updateDate;
	 * }
	 */

}
