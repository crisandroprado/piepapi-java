package com.piep.piepapi.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "chapters_chapter")
@Data
public class Chapter {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "name", length = 50)
	@Size(max = 50)
	private String name;

	@Column(name = "designation", length = 255)
	@Size(max = 255)
	private String designation;

	@Column(name = "contact_email", length = 50)
	@Size(max = 50)
	private String contactEmail;

	@Column(name = "contact_phone", length = 50)
	@Size(max = 50)
	private String contactPhone;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "president_id")
	private Integer presidentId;

	@Transient
	@OneToOne
	@JoinColumn(name = "president_id", referencedColumnName = "id", insertable = false, updatable = false)
	private User user;
}
