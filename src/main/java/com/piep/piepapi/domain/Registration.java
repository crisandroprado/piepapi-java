package com.piep.piepapi.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "events_registration")
@Data
public class Registration {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "event_id")
	private int eventId;

	@Column(name = "payment_id")
	private int paymentId;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "companions")
	private String companions;

	@Column(name = "number")
	private int number;

	@Column(name = "is_additional")
	@Range(min = 0, max = 1)
	private int isAdditional = 0;

	@Column(name = "email", length = 100)
	@Size(max = 100)
	private String email;

	@Column(name = "fee")
	private int fee;

	@Transient
	@OneToOne
	@JoinColumn(name = "event_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Event event;

	@Transient
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "payment_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Payment payment;

	/*@Transient
	private List<String> companionList = new ArrayList<String>();

	public List<String> getCompanionList() {
		if (!companions.equals(""))
			companionList = Arrays.asList(companions.split("**"));
		return companionList;
	}

	public void setCompanionList(List<String> companionList) {
		this.companionList = companionList;
	}

	@Transient
	private String paymentLink;

	public String getPaymentLink() {
		paymentLink = String.format("http://app.piep.org.ph/membership/payment/%s/", paymentId);
		return paymentLink;
	}

	public void setPaymentLink(String paymentLink) {
		this.paymentLink = paymentLink;
	}

	@Transient
	private String fullname;

	public String getFullname() {
		if (event != null && event.getUser() != null)
			fullname = String.format("%s %s %s", event.getUser().getFirstName(), event.getUser().getMiddleName(),
					event.getUser().getLastName());
		if (fullname.equals(""))
			fullname = email;
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	@Transient
	private String toEmail;

	/*public String getToEmail() {
		if (event != null && event.getUser() != null) {
			toEmail = event.getUser().getEmail();
		} else
			toEmail = email;

		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}*/

}
