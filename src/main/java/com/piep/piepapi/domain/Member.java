package com.piep.piepapi.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Table(name = "users_user")
@Data
public class Member implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	@JsonProperty("id")
	private int id;

	@Column(name = "password", length = 128)
	@Size(max = 128)
	@JsonProperty("password")
	private String password;

	@Column(name = "last_login")
	@JsonProperty("last_login")
	private Date lastLogin;

	@Column(name = "is_superuser")
	@JsonProperty("is_superuser")
	@Range(min = 0, max = 1)
	private int isSuperuser = 0;

	@Column(name = "username", length = 150)
	@JsonProperty("username")
	@Size(max = 150)
	private String username;

	@Column(name = "first_name", length = 30)
	@JsonProperty("first_name")
	@Size(max = 30)
	private String firstName;

	@Column(name = "last_name", length = 150)
	@JsonProperty("last_name")
	@Size(max = 150)
	private String lastName;

	@Column(name = "email", length = 254)
	@JsonProperty("email")
	@Size(max = 254)
	private String email;

	@Column(name = "is_staff")
	@Range(min = 0, max = 1)
	@JsonProperty("is_staff")
	private int isStaff = 0;

	@Column(name = "is_active")
	@Range(min = 0, max = 1)
	@JsonProperty("is_active")
	private int isActive = 1;

	@Column(name = "date_joined")
	@JsonProperty("date_joined")
	private Date dateJoined;

	@Column(name = "prc_license_number", length = 50)
	@Size(max = 50)
	@JsonProperty("prc_license_number")
	private String prcLicenseNumber;

	@Column(name = "year_obtained", length = 10)
	@Size(max = 10)
	@JsonProperty("year_obtained")
	private String yearObtained;

	@Column(name = "home_address", length = 255)
	@Size(max = 255)
	@JsonProperty("home_address")
	private String homeAddress;

	@Column(name = "office_address", length = 255)
	@Size(max = 255)
	@JsonProperty("office_address")
	private String officeAddress;

	@Column(name = "phone_number", length = 20)
	@Size(max = 20)
	@JsonProperty("phone_number")
	private String phoneNumber;

	@Column(name = "mobile_number", length = 20)
	@Size(max = 20)
	@JsonProperty("mobile_number")
	private String mobileNumber;

	@Column(name = "birthdate")
	@JsonProperty("birthdate")
	private Date birthdate;

	@Column(name = "educations")
	@JsonProperty("educations")
	private String educations;

	@Column(name = "employments")
	@JsonProperty("employments")
	private String employments;

	@Column(name = "industries")
	@JsonProperty("industries")
	private String industries;

	@Column(name = "skills")
	@JsonProperty("skills")
	private String skills;

	@Column(name = "certificates")
	@JsonProperty("certificates")
	private String certificates;

	@Column(name = "organizations")
	@JsonProperty("organizations")
	private String organizations;

	@Column(name = "middle_name", length = 255)
	@Size(max = 255)
	@JsonProperty("middle_name")
	private String middleName;

	@Column(name = "chapter_id")
	@JsonProperty("chapter_id")
	private Integer chapterId;

	@Column(name = "membership", length = 50)
	@Size(max = 50)
	@JsonProperty("membership")
	private String membership = "REGULAR";

	@Column(name = "membership_year", length = 20)
	@Size(max = 20)
	@JsonProperty("membership_year")
	private String membershipYear;

	@Column(name = "id_picture", length = 100)
	@Size(max = 100)
	@JsonProperty("id_picture")
	private String idPicture;

	@Column(name = "password_token")
	@JsonProperty("password_token")
	private String passwordToken;

	@Column(name = "downloaded_picture")
	@Range(min = 0, max = 1)
	@JsonProperty("downloaded_picture")
	private int downloadedPicture = 0;

	@Column(name = "email_sent")
	@Range(min = 0, max = 1)
	@JsonProperty("email_sent")
	private int emailSent = 0;

	@Column(name = "is_approved")
	@Range(min = 0, max = 1)
	@JsonProperty("is_approved")
	private int isApproved = 0;

	@Column(name = "amount_due")
	@JsonProperty("amount_due")
	private Double amountDue;
	
	@Column(name = "rfid", length = 50)
	@Size(max = 50)
	@JsonProperty("rfid")
	private String rfid;
	
	@Column(name = "for_approval")
	@Range(min = 0, max = 1)
	@JsonProperty("for_approval")
	private int forApproval = 0;
	
	@Column(name = "existing_member ")
	@Range(min = 0, max = 1)
	@JsonProperty("existing_member")
	private int existingMember  = 0;
	
	@Transient
	@OneToMany
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	@JsonProperty("member_balance")
	private List<MemberBalanceHeader> memberBalance = new ArrayList<MemberBalanceHeader>();

	/*@OneToOne
	@JoinColumn(name = "chapter_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Chapter chapter;

	@Transient
	@OneToOne
	@JoinColumn(name = "id", referencedColumnName = "user_id", insertable = false, updatable = false)
	private UserGroup userGroup;*/

}
