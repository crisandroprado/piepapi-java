package com.piep.piepapi.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "payments_member_balance")
@Data
public class MemberBalanceHeader {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "description", length = 200)
	@Size(max = 200)
	private String description;

	@Column(name = "user_id ")
	private Integer userId;
	
	@Column(name = "event_id ")
	private Integer eventId;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "is_paid")
	@Range(min = 0, max = 1)
	private int isPaid = 0;
	
	@Column(name = "is_membership")
	@Range(min = 0, max = 1)
	private int isMembership = 0;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updateDate;

	@Column(name = "created_by", length = 200)
	@Size(max = 200)
	private String createdBy;

	@Column(name = "updatedBy", length = 200)
	@Size(max = 200)
	private String updatedBy;
}
