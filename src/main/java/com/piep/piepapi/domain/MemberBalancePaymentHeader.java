package com.piep.piepapi.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Entity
@Table(name = "payments_member_balance_payment")
@Data
public class MemberBalancePaymentHeader {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "balance_id")
	private int balanceId;
	
	@Column(name = "payment_id")
	private int paymentId;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "created_by", length = 200)
	@Size(max = 200)
	private String createdBy;
}
