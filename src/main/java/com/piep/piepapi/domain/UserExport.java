package com.piep.piepapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.validator.constraints.Range;

import lombok.Data;

@Entity
@Table(name = "users_user")
@Data
public class UserExport implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "first_name", length = 30)
	@Size(max = 30)
	private String firstName;

	@Column(name = "last_name", length = 150)
	@Size(max = 150)
	private String lastName;

	@Column(name = "email", length = 254)
	@Size(max = 254)
	private String email;

	@Column(name = "prc_license_number", length = 50)
	@Size(max = 50)
	private String prcLicenseNumber;

	@Column(name = "middle_name", length = 255)
	@Size(max = 255)
	private String middleName;

	@Column(name = "id_picture", length = 100)
	@Size(max = 100)
	private String idPicture;

	@Column(name = "rfid", length = 50)
	@Size(max = 50)
	private String rfid;
	
	@Column(name = "chapter_id")
	private Integer chapterId;
	
	@Column(name = "skills")
	private String skills;

	@Column(name = "industries")
	private String industries;
	
	@Column(name = "year_obtained", length = 10)
	@Size(max = 10)
	private String yearObtained;
	
	@Column(name = "is_approved")
	@Range(min = 0, max = 1)
	private int isApproved = 0;
	
	@Transient
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "chapter_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Chapter chapter;

}
