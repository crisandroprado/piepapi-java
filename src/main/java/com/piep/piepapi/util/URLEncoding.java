package com.piep.piepapi.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.springframework.stereotype.Component;

@Component
public class URLEncoding {

	public String encode(String url) throws UnsupportedEncodingException {
		String fileName = "";
		if (url.contains(".")) {
			fileName = url.substring(url.lastIndexOf("/") + 1);
		}
		if (fileName.isEmpty())
			return url;

		String fileNameEncode = URLEncoder.encode(fileName, "UTF-8");
		fileNameEncode = fileNameEncode.replace("+", "%20");
		return url.replace(fileName, fileNameEncode);
	}
}
