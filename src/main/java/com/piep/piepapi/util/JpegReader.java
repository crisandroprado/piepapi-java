package com.piep.piepapi.util;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.springframework.stereotype.Component;

@Component
public class JpegReader {
	
	public BufferedImage readImage(InputStream stream, String format) throws IOException {
		Iterator<ImageReader> imageReaders = ImageIO.getImageReadersBySuffix(format);
		ImageReader imageReader = imageReaders.next();
		ImageInputStream iis = ImageIO.createImageInputStream(stream);
		imageReader.setInput(iis, true, true);
		Raster raster = imageReader.readRaster(0, null);
		int w = raster.getWidth();
		int h = raster.getHeight();

		BufferedImage result = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		int[] rgb = new int[3];
		int[] pixel = new int[3];
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				raster.getPixel(x, y, pixel);
				int cy = pixel[0];
				int cr = pixel[1];
				int cb = pixel[2];
				toRGB(cy, cb, cr, rgb);
				int r = rgb[0];
				int g = rgb[1];
				int b = rgb[2];
				int bgr = ((b & 0xFF) << 16) | ((g & 0xFF) << 8) | (r & 0xFF);
				result.setRGB(x, y, bgr);
			}
		}
		return result;
	}

	// Based on http://www.equasys.de/colorconversion.html
	private static void toRGB(int y, int cb, int cr, int[] rgb) {
		float fcy = y / 255.0f;
		float fcb = (cb - 128) / 255.0f;
		float fcr = (cr - 128) / 255.0f;

		float fr = fcy + 1.4f * fcr;
		float fg = fcy - 0.343f * fcb - 0.711f * fcr;
		float fb = fcy + 1.765f * fcb;

		fr = Math.min(1.0f, Math.max(0.0f, fr));
		fg = Math.min(1.0f, Math.max(0.0f, fg));
		fb = Math.min(1.0f, Math.max(0.0f, fb));

		int r = (int) (fr * 255);
		int g = (int) (fg * 255);
		int b = (int) (fb * 255);

		rgb[0] = r;
		rgb[1] = g;
		rgb[2] = b;
	}

}

