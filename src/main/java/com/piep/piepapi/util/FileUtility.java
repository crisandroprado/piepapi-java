package com.piep.piepapi.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;

import com.opencsv.CSVReader;
import com.piep.piepapi.domain.UserAttendance;
import com.piep.piepapi.domain.UserExport;
import com.piep.piepapi.email.model.ExcelFile;
import com.piep.piepapi.email.model.MemberDues;
import com.piep.piepapi.email.model.MemberData;

public class FileUtility {

	private static Logger logger = LoggerFactory.getLogger(FileUtility.class);

	public List<UserAttendance> decodeBase64AttendanceData(int eventId, ExcelFile excelFile) {

		try {

			List<UserAttendance> userAttendance = new ArrayList<UserAttendance>();

			// String decodeTempFilename = "/home/piepadmin/piepapijava/Attendance.xls";

			// File decodeTempFile = new File(decodeTempFilename);

			logger.info("Converting base64String to Bytes");
			byte[] csvBytes = Base64.decodeBase64(excelFile.getFileAsBase64String());

			logger.info("Converting bytes to csv reader");
			CSVReader reader = new CSVReader(new InputStreamReader(new ByteArrayInputStream(csvBytes)));

			logger.info("read csv file");

			String[] nextRecord;
			boolean firtRecord = true;
			while ((nextRecord = reader.readNext()) != null) {
				if (firtRecord) {
					firtRecord = false;
					continue;
				}
				if (nextRecord[0] == null || nextRecord[0].equals(""))
					break;
				UserAttendance attendance = new UserAttendance();
				attendance.setRfid(nextRecord[0].trim());
				attendance.setCount(Integer.parseInt(nextRecord[1].trim()));
				attendance.setTimeOut(nextRecord[2].trim());
				attendance.setDateOut(nextRecord[3].trim());
				attendance.setTimeIn(nextRecord[4].trim());
				attendance.setDateIn(nextRecord[5].trim());
				attendance.setEventId(eventId);
				logger.info("Attendance" + attendance.toString());
				userAttendance.add(attendance);
			}

			reader.close();
			logger.info("done converting csv to attendance object ");
			return userAttendance;

		} catch (Exception e) {
			logger.error((e.getMessage()));
			throw new IllegalArgumentException(e.getMessage(), e);
		}

	}

	public List<MemberData> decodeBase64UserData(ExcelFile excelFile) {

		try {

			List<MemberData> userRfids = new ArrayList<MemberData>();

			logger.info("Converting base64String to Bytes");
			byte[] excelBytes = Base64.decodeBase64(excelFile.getFileAsBase64String());

			logger.info("Converting bytes to stream");
			InputStream stream = new ByteArrayInputStream(excelBytes);

			logger.info("Converting steam to workbook");
			Workbook workBook = new XSSFWorkbook(stream);
			Sheet sheet = null;

			logger.info("Getting sheet name");
			if (excelFile.getSheetName() == null || excelFile.getSheetName().equals(""))
				sheet = workBook.getSheetAt(0);
			else
				sheet = workBook.getSheet(excelFile.getSheetName());

			logger.info("SheetName:" + sheet.getSheetName());

			Iterator<Row> rowIterator = sheet.rowIterator();
			boolean firtRecord = true;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (firtRecord) {
					firtRecord = false;
					continue;
				}
				if (getCellValue(row.getCell(0)) == null || getCellValue(row.getCell(0)).equals(""))
					break;
				MemberData userRfid = new MemberData();
				userRfid.setRfid(getCellValue(row.getCell(0)));
				userRfid.setFullName(getCellValue(row.getCell(1)));
				userRfid.setPrcNumber(getCellValue(row.getCell(2)));
				logger.info("RFID" + userRfid.toString());
				userRfids.add(userRfid);
			}
			workBook.close();
			logger.info("done converting excel to rfid master file object ");
			return userRfids;

		} catch (Exception e) {
			logger.error((e.getMessage()));
			throw new IllegalArgumentException(e.getMessage(), e);
		}

	}

	public List<MemberDues> decodeBase64MemberDues(ExcelFile excelFile) {

		try {

			List<MemberDues> memberDues = new ArrayList<MemberDues>();

			logger.info("Converting base64String to Bytes");
			byte[] excelBytes = Base64.decodeBase64(excelFile.getFileAsBase64String());

			logger.info("Converting bytes to stream");
			InputStream stream = new ByteArrayInputStream(excelBytes);

			logger.info("Converting steam to workbook");
			Workbook workBook = new XSSFWorkbook(stream);
			Sheet sheet = null;

			logger.info("Getting sheet name");
			if (excelFile.getSheetName() == null || excelFile.getSheetName().equals(""))
				sheet = workBook.getSheetAt(0);
			else
				sheet = workBook.getSheet(excelFile.getSheetName());

			logger.info("SheetName:" + sheet.getSheetName());

			Iterator<Row> rowIterator = sheet.rowIterator();
			boolean skip = true;
			Row rowheader1 = null;
			Row rowheader2 = null;
			boolean merged = false;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (!merged && (getCellValue(row.getCell(1)) == null || getCellValue(row.getCell(1)).equals("")))
					break;

				if (!getCellValue(row.getCell(1)).equalsIgnoreCase(excelFile.getCellNameDescription()) && skip)
					continue;
				else if (getCellValue(row.getCell(1)).equalsIgnoreCase(excelFile.getCellNameDescription())) {
					rowheader1 = row;
					skip = false;
					merged = true;
					continue;
				} else if (merged) {
					rowheader2 = row;
					skip = false;
					merged = false;
					continue;
				}

				for (Integer i = excelFile.getFirstColumnIndex(); i <= excelFile.getLastColumnIndex(); i++) {
					if (!getCellValue(row.getCell(i)).equals("")
							&& StringUtils.isNumeric(getCellValue(row.getCell(i)).replace(",", ""))) {

						MemberDues memberDue = new MemberDues();

						String[] names = getCellValue(row.getCell(1)).split(",");
						memberDue.setLastName(names[0].trim());
						String firstName = "";
						int index = 0;
						if (names.length > 1) {
							names = names[1].trim().split(" ");
						}

						else {
							names = getCellValue(row.getCell(1)).split(" ");
							index = 1;
						}

						for (Integer nameIndex = index; nameIndex <= names.length - 1; nameIndex++) {
							String name = names[nameIndex];
							if (name.endsWith(".") || name.length() <= 1)
								break;
							if (name.length() > 2 || (name.length() <= 2 && !name.endsWith("."))) {
								firstName += name + " ";
							}
						}

						memberDue.setFirstName(firstName.trim());
						memberDue.setAmount(Double.parseDouble(getCellValue(row.getCell(i)).replace(",", "")));
						if (!getCellValue(rowheader1.getCell(i)).equals(""))
							memberDue.setDescription(getCellValue(rowheader1.getCell(i)));
						else
							memberDue.setDescription(getCellValue(rowheader2.getCell(i)));

						logger.info(memberDue.getLastName() + "-" + memberDue.getFirstName() + "-"
								+ memberDue.getDescription());
						memberDues.add(memberDue);
					}

				}
			}
			workBook.close();
			logger.info("done converting excel to member duues file object ");
			return memberDues;

		} catch (Exception e) {
			logger.error((e.getMessage()));
			throw new IllegalArgumentException(e.getMessage(), e);
		}

	}

	public Resource download(List<UserExport> users) throws IOException {
		String[] columns = { "First Name", "Middle Name", "Last Name", "Email", "PRC License Number", "Year Obtained",
				"Skills", "Industries", "Chapter" };

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();

		// Create a Sheet
		Sheet sheet = workbook.createSheet("Member");

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
		}

		// Create Cell Style for formatting Date
		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MMM-dd-yyyy"));

		// Create Other rows and cells with employees data
		int rowNum = 1;
		for (UserExport user : users) {
			Row row = sheet.createRow(rowNum++);

			row.createCell(0).setCellValue(user.getFirstName());
			row.createCell(1).setCellValue(user.getMiddleName());
			row.createCell(2).setCellValue(user.getLastName());
			row.createCell(3).setCellValue(user.getEmail());
			row.createCell(4).setCellValue(user.getPrcLicenseNumber());
			row.createCell(5).setCellValue(user.getYearObtained());
			//row.createCell(6).setCellValue(user.getRfid());

			row.createCell(6).setCellValue(removedJSONString(user.getSkills()));

			row.createCell(7).setCellValue(removedJSONString(user.getIndustries()));

			if (user.getChapter() != null)
				row.createCell(8).setCellValue(user.getChapter().getName());
			else
				row.createCell(8).setCellValue("");

		}

		// Resize all columns to fit the content size
		/*
		 * for (int i = 0; i < columns.length; i++) { sheet.autoSizeColumn(i); }
		 */

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			workbook.write(bos);
		} finally {
			bos.close();
		}
		byte[] bytes = bos.toByteArray();
		ByteArrayResource byteArrayResource = new ByteArrayResource(bytes, "Members List.xlsx");

		// Closing the workbook
		workbook.close();

		return byteArrayResource;

	}
	
	public List<MemberData> decodeBase64MemberData(ExcelFile excelFile) {

		try {

			List<MemberData> userRfids = new ArrayList<MemberData>();

			logger.info("Converting base64String to Bytes");
			byte[] excelBytes = Base64.decodeBase64(excelFile.getFileAsBase64String());

			logger.info("Converting bytes to stream");
			InputStream stream = new ByteArrayInputStream(excelBytes);

			logger.info("Converting steam to workbook");
			Workbook workBook = new XSSFWorkbook(stream);
			Sheet sheet = null;

			logger.info("Getting sheet name");
			if (excelFile.getSheetName() == null || excelFile.getSheetName().equals(""))
				sheet = workBook.getSheetAt(0);
			else
				sheet = workBook.getSheet(excelFile.getSheetName());

			logger.info("SheetName:" + sheet.getSheetName());

			Iterator<Row> rowIterator = sheet.rowIterator();
			boolean firtRecord = true;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (firtRecord) {
					firtRecord = false;
					continue;
				}
				if (getCellValue(row.getCell(0)) == null || getCellValue(row.getCell(0)).equals(""))
					break;
				MemberData userRfid = new MemberData();
				userRfid.setLastName(getCellValue(row.getCell(0)));
				userRfid.setFirstName(getCellValue(row.getCell(1)));
				userRfid.setMiddleName(getCellValue(row.getCell(2)));
				userRfid.setPrcNumber(getCellValue(row.getCell(3)));
				logger.info("RFID" + userRfid.toString());
				userRfids.add(userRfid);
			}
			workBook.close();
			logger.info("done converting excel to member master file object ");
			return userRfids;

		} catch (Exception e) {
			logger.error((e.getMessage()));
			throw new IllegalArgumentException(e.getMessage(), e);
		}

	}

	private String getCellValue(Cell cell) {

		if (cell == null)
			return "";

		switch (cell.getCellTypeEnum()) {
		case STRING:
			return cleanCellValue(cell.getStringCellValue());

		case NUMERIC:
			DataFormatter dataFormatter = new DataFormatter();
			return cleanCellValue(dataFormatter.formatCellValue(cell));
		case BOOLEAN:
			return cleanCellValue(String.valueOf(cell.getBooleanCellValue()));
		default:
			return "";
		}

	}

	private String cleanCellValue(String cellValue) {
		return Jsoup.parse(cellValue).text();
	}

	private String removedJSONString(String value) {
		if (value == null)
			return "";
		return value.replace("[", "").replace("]", "").replace("\"", "").replace("\\", "");
	}
}
