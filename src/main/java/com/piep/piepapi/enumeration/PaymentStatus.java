package com.piep.piepapi.enumeration;

public enum PaymentStatus {

    success("S"),
    Failed("F"),
    Pending("P"),
    Unknown("U"),
    Refund("R"),
    Chargeback("C"),
    Voided("V"),
    Authorized("A");
    
    private String value;

	PaymentStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
