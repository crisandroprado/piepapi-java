package com.piep.piepapi.specification;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;

import com.piep.piepapi.domain.Payment;
import com.piep.piepapi.domain.UserAttendance;

public class PaymentSpecification {

	public static Specification<Payment> withUserId(Integer userId) {

		if (userId == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("userId"), userId);
		}
	}

	public static Specification<Payment> withType(String type) {

		if (type == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("type"), type);
		}
	}

	public static Specification<Payment> withTransactionNumber(String transactionNumber) {

		if (transactionNumber == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("transactionNumber"), transactionNumber);
		}
	}

	public static Specification<Payment> withToken(String token) {

		if (token == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("token"), token);
		}
	}

	public static Specification<Payment> withReferenceNumber(String referenceNumber) {

		if (referenceNumber == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("referenceNumber"), referenceNumber);
		}
	}

	public static Specification<Payment> withStatus(String status) {

		if (status == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("status"), status);
		}
	}

	public static Specification<Payment> withAmount(Double amount) {

		if (amount == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("amount"), amount);
		}
	}

	public static Specification<Payment> withCreatedAt(Date createdAt) {

		if (createdAt == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("createdAt"), createdAt);
		}
	}

	public static Specification<Payment> withUpdatedAt(Date updatedAt) {

		if (updatedAt == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("updatedAt"), updatedAt);
		}
	}
	
	public static Specification<Payment> withMethod(String method) {

		if (method == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("method"), method);
		}
	}
}
