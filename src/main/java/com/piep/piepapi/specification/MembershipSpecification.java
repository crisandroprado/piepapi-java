package com.piep.piepapi.specification;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.Membership;
import com.piep.piepapi.domain.PaymentHeader;
import com.piep.piepapi.domain.User;


public class MembershipSpecification {

	public static Specification<Membership> withYearBetween(Integer startYear,Integer endYear) {

		if (startYear == null && endYear == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.between(root.get("year"), startYear,endYear);
		}
	}
	
	public static Specification<Membership> withFirstName(String firstName) {

		if (firstName == null) {
			return null;
		} else {
			return new Specification<Membership>() {
		        /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Predicate toPredicate(Root<Membership> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		            Join<Membership,User> user = root.join("user");
		            return cb.equal(user.get("firstName"), firstName);
		        }
		    };
		}
	}
	
	public static Specification<Membership> withLastName(String lastName) {

		if (lastName == null) {
			return null;
		} else {
			return new Specification<Membership>() {
		        /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Predicate toPredicate(Root<Membership> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		            Join<Membership,User> user = root.join("user");
		            return cb.equal(user.get("lastName"), lastName);
		        }
		    };
		}
	}
	
	public static Specification<Membership> withStatus(String status) {

		if (status == null) {
			return null;
		} else {
			return new Specification<Membership>() {
		        /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Predicate toPredicate(Root<Membership> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		            Join<Membership,PaymentHeader> payment = root.join("payment");
		            return cb.equal(payment.get("status"), status);
		        }
		    };
		}
	}
	

	public static Specification<Membership> withCreatedDateBetween(Date startDate,Date endDate) {

		if (startDate == null && endDate == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.between(root.get("createdDate"), startDate,endDate);
		}
	}
}
