package com.piep.piepapi.specification;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;

import com.piep.piepapi.domain.MemberBalancePayment;

public class MemberBalancePaymentSpecification {

	public static Specification<MemberBalancePayment> withBalanceId(Integer balanceId) {

		if (balanceId == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("balanceId"), balanceId);
		}
	}

	public static Specification<MemberBalancePayment> withPaymentId(Integer paymentId) {

		if (paymentId == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("paymentId"), paymentId);
		}
	}

	public static Specification<MemberBalancePayment> withCreatedDate(Date createdDate) {

		if (createdDate == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("createdDate"), createdDate);
		}
	}

	public static Specification<MemberBalancePayment> withCreatedBy(String createdBy) {

		if (createdBy == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("createdBy"), createdBy);
		}
	}

	
}
