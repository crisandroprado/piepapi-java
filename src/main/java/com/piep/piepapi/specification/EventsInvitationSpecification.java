package com.piep.piepapi.specification;

import org.springframework.data.jpa.domain.Specification;

import com.piep.piepapi.domain.EventsInvitation;

public class EventsInvitationSpecification {

	public static Specification<EventsInvitation> withEventId(Integer eventId) {

		if (eventId == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("eventId"), eventId);
		}
	}

	public static Specification<EventsInvitation> withUserId(Integer userId) {

		if (userId == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("userId"), userId);
		}
	}

}
