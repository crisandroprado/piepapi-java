package com.piep.piepapi.specification;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;

import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.UserExport;


public class UserExportSpecification {

	public static Specification<UserExport> withChapterId(Integer chapterId) {

		if (chapterId == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("chapterId"), chapterId);
		}
	}
	
	public static Specification<UserExport> withFirstName(String firstName) {

		if (firstName == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("firstName"), firstName);
		}
	}

	public static Specification<UserExport> withLastName(String lastName) {

		if (lastName == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("lastName"), lastName);
		}
	}
	
	public static Specification<UserExport> withSkills(String skills) {

		if (skills == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("skills"), skills);
		}
	}
	
	public static Specification<UserExport> withIndustries(String industries) {

		if (industries == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("industries"), industries);
		}
	}
	
	public static Specification<UserExport> withPrcLicenseNumber(String prcLicenseNumber) {

		if (prcLicenseNumber == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("prcLicenseNumber"), prcLicenseNumber);
		}
	}
	
	public static Specification<UserExport> withYearObtained(String yearObtained) {

		if (yearObtained == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("yearObtained"), yearObtained);
		}
	}
	
	public static Specification<UserExport> withIsApproved(Integer isApproved) {

		if (isApproved == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("isApproved"), isApproved);
		}
	}
	
}
