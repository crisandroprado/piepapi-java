package com.piep.piepapi.specification;

import org.springframework.data.jpa.domain.Specification;

import com.piep.piepapi.domain.UserAttendance;

public class UserAttendanceSpecification {

	public static Specification<UserAttendance> withDateIn(String dateIn) {

		if (dateIn == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("dateIn"), dateIn);
		}
	}

	public static Specification<UserAttendance> withDateOut(String dateOut) {

		if (dateOut == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("dateOut"), dateOut);
		}
	}
	
	public static Specification<UserAttendance> withEventId(Integer eventId) {

		if (eventId == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("eventId"), eventId);
		}
	}
}
