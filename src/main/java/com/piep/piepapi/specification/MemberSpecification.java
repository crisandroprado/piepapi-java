package com.piep.piepapi.specification;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;

import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.MemberBalance;


public class MemberSpecification {

	public static Specification<Member> withChapterId(Integer chapterId) {

		if (chapterId == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("chapterId"), chapterId);
		}
	}
	
	public static Specification<Member> withFirstName(String firstName) {

		if (firstName == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("firstName"), firstName);
		}
	}

	public static Specification<Member> withLastName(String lastName) {

		if (lastName == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("lastName"), lastName);
		}
	}
	
	public static Specification<Member> withSkills(String skills) {

		if (skills == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("skills"), skills);
		}
	}
	
	public static Specification<Member> withIndustries(String industries) {

		if (industries == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("industries"), industries);
		}
	}
	
	public static Specification<Member> withPrcLicenseNumber(String prcLicenseNumber) {

		if (prcLicenseNumber == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("prcLicenseNumber"), prcLicenseNumber);
		}
	}
	
	public static Specification<Member> withYearObtained(String yearObtained) {

		if (yearObtained == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("yearObtained"), yearObtained);
		}
	}
	
	public static Specification<Member> withIsApproved(Integer isApproved) {

		if (isApproved == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("isApproved"), isApproved);
		}
	}
	
	public static Specification<Member> withMiddleName(String middleName) {

		if (middleName == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("middleName"), middleName);
		}
	}
	
	public static Specification<Member> withEmail(String email) {

		if (email == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("email"), email);
		}
	}
	
	public static Specification<Member> withCreatedDateBetween(Date startDate, Date endDate) {

		if (startDate == null || endDate == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.between(root.get("dateJoined"), startDate,endDate);
		}
	}
	
}
