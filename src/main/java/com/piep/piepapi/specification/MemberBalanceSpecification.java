package com.piep.piepapi.specification;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.User;


public class MemberBalanceSpecification {

	public static Specification<MemberBalance> withUserId(Integer userId) {

		if (userId == null || userId == 0) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("userId"), userId);
		}
	}
	
	public static Specification<MemberBalance> withDescription(String description) {

		if (description == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("description"), description);
		}
	}

	public static Specification<MemberBalance> withIsPaid(Integer isPaid) {

		if (isPaid == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("isPaid"), isPaid);
		}
	}
	
	public static Specification<MemberBalance> withCreatedDateBetween(Date startDate, Date endDate) {

		if (startDate == null || endDate == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.between(root.get("createdDate"), startDate,endDate);
		}
	}
	
	public static Specification<MemberBalance> withAmountBetween(Double startAmount, Double endAmount) {

		if (startAmount == null || endAmount == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.between(root.get("amount"), startAmount,endAmount);
		}
	}
	
	
	public static Specification<MemberBalance> joinUserChaptedId(Integer chapterId) {
	    
		if (chapterId == null) {
			return null;
		} else {
			return new Specification<MemberBalance>() {
		        /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Predicate toPredicate(Root<MemberBalance> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		            Join<MemberBalance,User> user = root.join("user");
		            return cb.equal(user.get("chapterId"), chapterId);
		        }
		    };
		}
	}
	
	public static Specification<MemberBalance> withIsMembership(Integer isMembership) {

		if (isMembership == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("isMembership"), isMembership);
		}
	}
	
	
	
	
	/*public static Specification<MemberBalance> withUpdatedDate(Date updatedDate) {

		if (updatedDate == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("updatedDate"), updatedDate);
		}
	}
	
	public static Specification<MemberBalance> withCreatedBy(String createdBy) {

		if (createdBy == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("createdBy"), createdBy);
		}
	}
	
	public static Specification<MemberBalance> withUpdatedBy(String updatedBy) {

		if (updatedBy == null) {
			return null;
		} else {
			return (root, query, cb) -> cb.equal(root.get("updatedBy"), updatedBy);
		}
	}*/
}
