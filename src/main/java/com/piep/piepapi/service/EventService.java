package com.piep.piepapi.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.Event;
import com.piep.piepapi.email.model.SocialMediaBanner;

public interface EventService {

	Page<Event> find(Date end,
			    Pageable pageable) throws Exception;
	
	Page<Event> findAll(Pageable pageable) throws Exception;
	
	Event create(Event event) throws Exception;
	
	Event findById(int id) throws Exception;
	
	Event update(int id, Event event) throws Exception;

	Event delete(int id) throws Exception;
	
	Event update(int id, SocialMediaBanner updates) throws Exception;
	
	Resource download(int id) throws Exception;
	
	Page<Event> findPastEvent(Date end, Pageable pageable) throws Exception;
	
}
