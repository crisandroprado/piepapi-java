package com.piep.piepapi.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalanceHeader;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.email.model.MemberDues;

public interface MemberBalanceService {

	Page<MemberBalance> findAll(Integer userId,Integer chapterId, String description, Integer isPaid,Integer isMembership, Date startDate, Date endDate,
			Double startAmount, Double endAmount, Pageable pageable) throws Exception;

	Page<MemberBalanceHeader> create(List<MemberBalanceHeader> memberBalance, Pageable pageable) throws Exception;

	MemberBalance findById(int id) throws Exception;

	MemberBalanceHeader update(MemberBalanceHeader memberBalance) throws Exception;

	MemberBalanceHeader delete(int id) throws Exception;
	
	GeneralResponse sendManualEmailPayment(int paymentId) throws Exception;
	
	List<MemberBalance> findByMemberBalancePaymentPaymentId(int paymentId);
	
	Page<MemberBalanceHeader> upload(List<MemberDues> memberBalance, Pageable pageable) throws Exception;
	
	MemberBalanceHeader update(int paymendId) throws Exception;
	
	Page<MemberBalanceHeader> membership(String year, Double amount, Pageable pageable) throws Exception;
}
