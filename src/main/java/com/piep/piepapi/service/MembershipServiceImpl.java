package com.piep.piepapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.piep.piepapi.controller.MemberBalanceController;
import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalanceHeader;
import com.piep.piepapi.domain.Membership;
import com.piep.piepapi.domain.User;
import com.piep.piepapi.email.model.EmailMessage;
import com.piep.piepapi.email.model.TemplateParams;
import com.piep.piepapi.email.model.TemplateResult;
import com.piep.piepapi.email.service.EmailService;
import com.piep.piepapi.email.service.TemplateEngine;
import com.piep.piepapi.repository.MembershipRepository;
import com.piep.piepapi.repository.UserRepository;
import com.piep.piepapi.specification.MembershipSpecification;

@Service
public class MembershipServiceImpl implements MembershipService {
	
	private static Logger logger = LoggerFactory.getLogger(MembershipServiceImpl.class);

	@Autowired
	MembershipRepository membershipRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private TemplateEngine templateEngine;
	
	@Autowired
	private EmailService emailService;
	

	@Override
	public Page<Membership> findAll(Integer startYear, Integer endYear, String firstName, String lastName,
			String status, Date startDate, Date endDate, Pageable pageable) throws Exception {
		return membershipRepository.findAll(Specification
				.where(MembershipSpecification.withCreatedDateBetween(startDate, endDate))
				.and(MembershipSpecification.withFirstName(firstName))
				.and(MembershipSpecification.withLastName(lastName)).and(MembershipSpecification.withStatus(status))
				.and(MembershipSpecification.withYearBetween(startYear, endYear)), pageable);
	}

	@Override
	public Page<Membership> createAll(List<Membership> memberships,Pageable pageable) throws Exception {
		for(Membership membership:memberships) {
			if(membership.getPayment() == null)
				throw new Exception("Payment was null");
		}
		List<Membership> members=  membershipRepository.saveAll(memberships);
		/*for(Membership membership:members) {
			sendEmail(membership);
		}*/
		
		long totalRows = 0;
		if (members != null && members.size() > 0)
			totalRows = members.size();

		
		Page<Membership> membersHeadersPage = new PageImpl(members, pageable, totalRows);

		return membersHeadersPage;
		
		//return members;
	}

	@Override
	public Membership findById(int id) throws Exception {
		return membershipRepository.findById(id);
	}

	@Override
	public Membership update(Membership memberships) throws Exception {
		return membershipRepository.save(memberships);
	}

	@Override
	public Membership delete(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Membership create(Membership memberships) throws Exception {
		if(memberships.getPayment() == null)
			throw new Exception("Payment was null");
		Membership member=  membershipRepository.save(memberships);
		//sendEmail(memberships);
		return member;
	}
	
	private void sendEmail(Membership membership) throws Exception {
		try{
			
			String paymentDescription =  membership.getPayment().getDescription();
			if(paymentDescription.equals(null)){
				logger.info("Email was not sent, payment description is null for payment id " + membership.getPayment().getId() );
			}
			
			TemplateParams templateParams = new TemplateParams();
			String currency =  membership.getPayment().getCurrency();
			List<String> descriptions = new ArrayList<String>();

			String description = String.format("%s %s: %s %s",Integer.toString(membership.getYear()), "Membership fee", currency,
					membership.getPayment().getAmount().toString());
			descriptions.add(description);
			templateParams.addParam("descriptions", descriptions);
			templateParams.addParam("paymentDescriptions",paymentDescription);
			templateParams.addParam("fullName", membership.getUser().getFullname());
			templateParams.addParam("total", membership.getPayment().getAmount());
			templateParams.addParam("paymentUrl", "http://app.piep.org.ph/membership/payment/" + membership.getPayment().getId() + "/");
			templateParams.addParam("currency",currency);
			
			TemplateResult templateResult = new TemplateResult();
			templateResult = templateEngine.process("manual_payment", templateParams);
			EmailMessage emailMsg = new EmailMessage();
			emailMsg.setFromEmail("support@piep.org.ph");
		    emailMsg.setSubject(templateResult.getSubject());
			emailMsg.setBody(templateResult.getBody());
			List<String> toEmails = new ArrayList<String>();
			toEmails.add(membership.getUser().getEmail());
			logger.info("Membership email was sent to: " + toEmails.toString());
			emailMsg.setToEmails(toEmails);
			emailService.send(emailMsg);
		}
		catch(Exception ex) {
			throw new Exception(ex);
		}
	}

}
