package com.piep.piepapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.piep.piepapi.domain.Chapter;
import com.piep.piepapi.repository.ChapterRepository;

@Service
public class ChapterServiceImpl implements ChapterService {

	@Autowired 
	ChapterRepository chapterRepository;
	
	@Override
	public Page<Chapter> findAll(Pageable pageable) throws Exception {
		return chapterRepository.findAll(pageable);
	}

	@Override
	public Chapter create(Chapter chapter) throws Exception {
		chapterRepository.save(chapter);
		return chapter;
	}

	@Override
	public Chapter findById(int id) throws Exception {
		return chapterRepository.findById(id);
	}

	@Override
	public Chapter update(Chapter chapter) throws Exception {
		chapterRepository.save(chapter);
		return chapter;
	}

	@Override
	public Chapter delete(int id) throws Exception {
		Chapter chapter = findById(id);
		if(chapter!= null)
			chapterRepository.delete(chapter);
		return chapter;
	}

	
}
