package com.piep.piepapi.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.piep.piepapi.domain.Event;
import com.piep.piepapi.email.model.SocialMediaBanner;
import com.piep.piepapi.repository.EventRepository;

import javassist.NotFoundException;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private TransferService transferService;

	@Override
	public Page<Event> find(Date end, Pageable pageable) throws Exception {
		return eventRepository.findByEndDateGreaterThan(end, pageable);
	}

	@Override
	public Page<Event> findAll(Pageable pageable) throws Exception {
		return eventRepository.findAll(pageable);
	}

	@Override
	public Event create(Event event) throws Exception {

		eventRepository.save(event);
		return event;
	}

	@Override
	public Event update(int id, Event event) throws Exception {
		eventRepository.save(event);
		return event;
	}

	@Override
	public Event delete(int id) throws Exception {
		Event event = findById(id);
		if (event != null)
			eventRepository.delete(event);
		return event;
	}

	@Override
	public Event findById(int id) throws Exception {
		return eventRepository.findById(id);
	}

	@Override
	public Event update(int id, SocialMediaBanner updates) throws Exception {
		Event event = eventRepository.findById(id);
		if (event != null) {
			transferService.upload(updates);
			event.setSocialMediaBanner(updates.getFileName());
			eventRepository.save(event);
		}
		return event;
	}

	@Override
	public Resource download(int id) throws Exception {
		Event event = eventRepository.findById(id);

		if (event == null)
			throw new NotFoundException("Event with ID " + Integer.toString(id) + " does not exist.");

		if (event.getSocialMediaBanner() == null)
			throw new NotFoundException(
					"Social Media Banner in Event with ID " + Integer.toString(id) + " does not exist.");

		byte[] downloaded = transferService.download(event.getSocialMediaBanner());
		ByteArrayResource byteArrayResource = new ByteArrayResource(downloaded, event.getSocialMediaBanner());
		return byteArrayResource;
	}
	
	@Override
	public Page<Event> findPastEvent(Date end, Pageable pageable) throws Exception {
		return eventRepository.findByEndDateLessThan(end, pageable);
	}

}
