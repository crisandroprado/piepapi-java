package com.piep.piepapi.service;

import java.io.IOException;
import java.nio.ByteBuffer;

import com.piep.piepapi.email.model.SocialMediaBanner;

public interface TransferService {

	void upload(SocialMediaBanner socialMediaBanner) throws Exception;

	byte[] download(String filename) throws Exception;
}
