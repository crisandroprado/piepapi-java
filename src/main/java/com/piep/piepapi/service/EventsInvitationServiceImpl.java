package com.piep.piepapi.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.piep.piepapi.configuration.Config;
import com.piep.piepapi.domain.EventsInvitation;
import com.piep.piepapi.email.model.EmailAttachment;
import com.piep.piepapi.email.model.EmailMessage;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.email.model.TemplateParams;
import com.piep.piepapi.email.model.TemplateResult;
import com.piep.piepapi.email.service.EmailService;
import com.piep.piepapi.email.service.TemplateEngine;
import com.piep.piepapi.repository.EventsInvitationRepository;
import com.piep.piepapi.specification.EventsInvitationSpecification;

@Service
public class EventsInvitationServiceImpl implements EventsInvitationService {

	@Autowired
	EventsInvitationRepository eventsInvitationRepository;
	
	@Autowired
	private TemplateEngine templateEngine;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private Config config;
	
	@Override
	public Page<EventsInvitation> findAll(Integer eventId, Integer userId, Pageable pageable) throws Exception {
		return eventsInvitationRepository
				.findAll(Specification.where(EventsInvitationSpecification.withEventId(eventId))
						.and(EventsInvitationSpecification.withUserId(userId)), pageable);
	}

	@Override
	public EventsInvitation create(EventsInvitation eventsInvitation) throws Exception {
		return eventsInvitationRepository.save(eventsInvitation);
	}

	@Override
	public EventsInvitation findById(int id) throws Exception {
		return eventsInvitationRepository.findById(id);
	}

	@Override
	public EventsInvitation update(EventsInvitation eventsInvitation) throws Exception {
		return eventsInvitationRepository.save(eventsInvitation);
	}

	@Override
	public EventsInvitation delete(int id) throws Exception {
		EventsInvitation eventsInvitation = eventsInvitationRepository.findById(id);
		if (eventsInvitation != null)
			eventsInvitationRepository.delete(eventsInvitation);
		return eventsInvitation;
	}

	@Override
	public GeneralResponse sendEventsInvitationEmail(int id) {
		GeneralResponse generalResponse = new GeneralResponse();
		try{
			EventsInvitation eventsInvitation = eventsInvitationRepository.findById(id);
			if(eventsInvitation !=null) {
				TemplateParams templateParams = new TemplateParams();
				templateParams.addParam("last_name", eventsInvitation.getUser().getLastName());
				TemplateResult templateResult = new TemplateResult();
				templateResult = templateEngine.process("events_invitation", templateParams);
				
				TemplateParams templateAttachParams = new TemplateParams();
				LocalDate now = LocalDate.now();
				templateAttachParams.addParam("date_created", now.toString());
				templateAttachParams.addParam("addresse_name", eventsInvitation.getAddresseFullName());
				templateAttachParams.addParam("last_name", eventsInvitation.getUser().getLastName());
				TemplateResult templateAttachResult = new TemplateResult();
				templateAttachResult = templateEngine.process("events_invitation_to_pdf", templateAttachParams);
				
				writeToFile(templateAttachResult.getBody(),"EventInvitation.html");
				
				String filename = config.getEmailAttachment() + "EventInvitation.html";

				Document document = new Document();
			    PdfWriter writer = PdfWriter.getInstance(document,
			      new FileOutputStream(config.getEmailAttachment() + "EventInvitation.pdf"));
			    document.open();
			    XMLWorkerHelper.getInstance().parseXHtml(writer, document,
			      new FileInputStream(filename));
			    document.close();
			
			    Path path = Paths.get(config.getEmailAttachment() + "EventInvitation.pdf");
	            byte[] eventInvitation = Files.readAllBytes(path);

	            path = Paths.get(config.getEmailAttachment() + "PIEP-NatCon-Briefer.pdf");
	            byte[] piepBriefer = Files.readAllBytes(path);
	            
	            EmailAttachment emailAttachment = new EmailAttachment();
	    		List<EmailAttachment> emailAttachments = new ArrayList<EmailAttachment>();
	    		
	    		emailAttachment.setContent(eventInvitation);
	    		emailAttachment.setName("EventInvitation.pdf");
	    		emailAttachments.add(emailAttachment);
	    		
	    		EmailAttachment emailAttachment2 = new EmailAttachment();
	    		emailAttachment2.setContent(piepBriefer);
	    		emailAttachment2.setName("PIEP-NatCon-Briefer.pdf");
	    		emailAttachments.add(emailAttachment2);
				
	    		
				EmailMessage emailMsg = new EmailMessage();
				emailMsg.setFromEmail("support@piep.org.ph");
			    emailMsg.setSubject(templateResult.getSubject());
				emailMsg.setBody(templateResult.getBody());
				//writeToFile(emailMsg.getBody(),"EventInvitationBody.html");
				List<String> toEmails = new ArrayList<String>();
				toEmails.add(eventsInvitation.getUser().getEmail());
				//toEmails.add("crisandro_prado@yahoo.com");
				emailMsg.setToEmails(toEmails);
				emailMsg.setAttachments(emailAttachments);
				emailService.send(emailMsg);
				generalResponse.setMessage("Email was sent to:" + eventsInvitation.getUser().getEmail());
				//generalResponse.setMessage("Email was sent to:" + "crisandro_prado@yahoo.com");
				generalResponse.setResponse("OK");
			}
			else {
				generalResponse.setMessage("Email was not sent, events invitation id " + id + " not found on Events_Invitation");
				generalResponse.setResponse("ERROR");
			}
		}
		catch(Exception ex) {
			generalResponse.setMessage(ex.getMessage());
			generalResponse.setResponse("ERROR");
		}
		return generalResponse;
	}
	
	private void writeToFile(String data, String filename) throws Exception {
		FileOutputStream outputStream = new FileOutputStream(config.getEmailAttachment() + filename);
		byte[] strToBytes = data.getBytes();
		outputStream.write(strToBytes);
		outputStream.close();
	}


}
