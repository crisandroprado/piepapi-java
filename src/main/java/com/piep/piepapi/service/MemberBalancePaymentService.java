package com.piep.piepapi.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.MemberBalancePayment;
import com.piep.piepapi.domain.MemberBalancePaymentHeader;
import com.piep.piepapi.email.model.GeneralResponse;

public interface MemberBalancePaymentService {

	Page<MemberBalancePayment> findAll(Integer balanceId, Integer paymentId, Date createdDate,
			String createdBy, Pageable pageable) throws Exception;

	Page<MemberBalancePaymentHeader> create(List<MemberBalancePaymentHeader> memberBalancePayment,Pageable pageable) throws Exception;

	MemberBalancePayment findById(int id) throws Exception;

	MemberBalancePaymentHeader update(MemberBalancePaymentHeader memberBalancePayment) throws Exception;

	MemberBalancePayment delete(int id) throws Exception;

	List<MemberBalancePayment> findByBalanceId(Integer balanceId) throws Exception;
}
