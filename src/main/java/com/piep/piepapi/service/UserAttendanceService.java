package com.piep.piepapi.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.UserAttendance;

public interface UserAttendanceService {

	Page<UserAttendance> findAll(String dateIn, String dateOut,Integer eventId, Pageable pageable) throws Exception;

	List<UserAttendance> create(List<UserAttendance> userAttendance) throws Exception;

	Page<UserAttendance> findByRfid(String rfid,Pageable pageable) throws Exception;

	List<UserAttendance> delete(String rfid) throws Exception;
	
	Page<UserAttendance> findByUserId(int id,Pageable pageable) throws Exception;

}
