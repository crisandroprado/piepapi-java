package com.piep.piepapi.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.UserExport;
import com.piep.piepapi.email.model.MemberData;

public interface MemberService {

	Page<Member> findAll(Integer chapterId, String firstName, String lastName, String skills, String industries,
			String prcLicenseNumber, String yearObtained, Integer isApproved,String middleName,String email, Date startDate, Date endDate, Pageable pageable) throws Exception;

	Member create(Member member) throws Exception;

	Member findById(int id) throws Exception;

	Member update(Member member) throws Exception;

	Member delete(int id) throws Exception;

	List<Member> update(List<MemberData> userRfid) throws Exception;

	Member update(int id, String rfid) throws Exception;
	
	List<UserExport> download(Integer chapterId, String firstName, String lastName, String skills, String industries,
			String prcLicenseNumber, String yearObtained, Integer isApproved) throws Exception;
}
