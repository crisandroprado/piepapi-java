package com.piep.piepapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalancePayment;
import com.piep.piepapi.domain.MemberBalancePaymentHeader;
import com.piep.piepapi.domain.Payment;
import com.piep.piepapi.enumeration.PaymentStatus;
import com.piep.piepapi.repository.MemberBalancePaymentHeaderRepository;
import com.piep.piepapi.repository.MemberBalancePaymentRepository;
import com.piep.piepapi.repository.MemberBalanceRepository;
import com.piep.piepapi.repository.PaymentRepository;
import com.piep.piepapi.specification.MemberBalancePaymentSpecification;

@Service
public class MemberBalancePaymentServiceImpl implements MemberBalancePaymentService {

	@Autowired
	MemberBalancePaymentRepository memberBalancePaymentRepository;

	@Autowired
	MemberBalancePaymentHeaderRepository memberBalancePaymentHeaderRepository;

	@Autowired
	MemberBalanceRepository memberBalanceRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Override
	public Page<MemberBalancePayment> findAll(Integer balanceId, Integer paymentId, Date createdDate, String createdBy,
			Pageable pageable) throws Exception {

		return memberBalancePaymentRepository
				.findAll(Specification.where(MemberBalancePaymentSpecification.withBalanceId(balanceId))
						.and(MemberBalancePaymentSpecification.withPaymentId(paymentId))
						.and(MemberBalancePaymentSpecification.withCreatedDate(createdDate))
						.and(MemberBalancePaymentSpecification.withCreatedBy(createdBy)), pageable);

	}

	@Override
	public Page<MemberBalancePaymentHeader> create(List<MemberBalancePaymentHeader> memberBalancePayment,
			Pageable pageable) throws Exception {
		List<MemberBalancePaymentHeader> memberBalancePaymentHeaders = new ArrayList<MemberBalancePaymentHeader>();
		for (MemberBalancePaymentHeader memberBalancePaymentHeader : memberBalancePayment) {
			MemberBalance memberBalance = memberBalanceRepository.findById(memberBalancePaymentHeader.getBalanceId());
			if (memberBalance == null)
				throw new Exception("Member Balance not found.");
			if (memberBalance.getIsPaid() == 1)
				throw new Exception("Member Balance was already paid.");

			Payment payment = paymentRepository.findById(memberBalancePaymentHeader.getPaymentId());

			if (payment == null)
				throw new Exception("Payment not found.");

			List<MemberBalancePayment> memberBalancePayments = memberBalancePaymentRepository
					.findByBalanceId(memberBalancePaymentHeader.getBalanceId());
			Double amountPaid = memberBalancePaymentHeader.getAmount();
			Integer isPaid = 1;
			if (memberBalancePayments != null) {
				for (MemberBalancePayment existingMemberBalancePayment : memberBalancePayments) {
					if(existingMemberBalancePayment.getPayment() != null) {
						if(!existingMemberBalancePayment.getPayment().getStatus().equalsIgnoreCase(PaymentStatus.success.getValue())) {
							isPaid = 0;
							break;
						}	
					}
				}
				//amountPaid += existingMemberBalancePayment.getAmount();
			}
			else
				isPaid = 0;
			
			//if(isPaid == 1) { //(memberBalance.getAmount() <= amountPaid) {

				memberBalance.setIsPaid(isPaid);
				memberBalanceRepository.save(memberBalance);
			//}

			memberBalancePaymentHeader.setCreatedDate(new Date());
			memberBalancePaymentHeaders.add(memberBalancePaymentHeader);

		}
		memberBalancePaymentHeaderRepository.saveAll(memberBalancePaymentHeaders);

		long totalRows = 0;
		if (memberBalancePaymentHeaders != null && memberBalancePaymentHeaders.size() > 0)
			totalRows = memberBalancePaymentHeaders.size();

		Page<MemberBalancePaymentHeader> memberBalancePaymentHeadersPage = new PageImpl(memberBalancePaymentHeaders,
				pageable, totalRows);

		return memberBalancePaymentHeadersPage;

		// return memberBalancePaymentHeaders;
	}

	@Override
	public MemberBalancePayment findById(int id) throws Exception {
		MemberBalancePayment memberBalancePayment = memberBalancePaymentRepository.findById(id);
		return memberBalancePayment;
	}

	@Override
	public MemberBalancePaymentHeader update(MemberBalancePaymentHeader memberBalancePayment) throws Exception {
		memberBalancePaymentHeaderRepository.save(memberBalancePayment);
		return memberBalancePayment;
	}

	@Override
	public MemberBalancePayment delete(int id) throws Exception {
		MemberBalancePayment paymentBalance = memberBalancePaymentRepository.findById(id);
		memberBalancePaymentRepository.delete(paymentBalance);
		return paymentBalance;
	}

	@Override
	public List<MemberBalancePayment> findByBalanceId(Integer balanceId) throws Exception {
		List<MemberBalancePayment> paymentBalance = memberBalancePaymentRepository.findByBalanceId(balanceId);
		return paymentBalance;
	}

}
