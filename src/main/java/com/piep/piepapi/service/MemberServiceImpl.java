package com.piep.piepapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.piep.piepapi.controller.MemberController;
import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.User;
import com.piep.piepapi.domain.UserExport;
import com.piep.piepapi.email.model.MemberData;
import com.piep.piepapi.repository.MemberRepository;
import com.piep.piepapi.repository.UserExportRepository;
import com.piep.piepapi.repository.UserRepository;
import com.piep.piepapi.specification.MemberBalanceSpecification;
import com.piep.piepapi.specification.MemberSpecification;
import com.piep.piepapi.specification.UserExportSpecification;

@Service
public class MemberServiceImpl implements MemberService {

	private static Logger logger = LoggerFactory.getLogger(MemberServiceImpl.class);
	/*@Autowired
	UserRepository userRepository;*/
	
	@Autowired
	MemberRepository memberRepository;
	
	@Autowired
	UserExportRepository userExportRepository;
	
	@Override
	public Page<Member> findAll(Integer chapterId, String firstName, String lastName, String skills, String industries,
			String prcLicenseNumber, String yearObtained, Integer isApproved,String middleName,String email, Date startDate, Date endDate,Pageable pageable) throws Exception {
		
		return memberRepository.findAll(Specification.where(MemberSpecification.withChapterId(chapterId))
				.and(MemberSpecification.withFirstName(firstName))
				.and(MemberSpecification.withIndustries(industries))
				.and(MemberSpecification.withLastName(lastName))
				.and(MemberSpecification.withPrcLicenseNumber(prcLicenseNumber))
				.and(MemberSpecification.withSkills(skills))
				.and(MemberSpecification.withYearObtained(yearObtained))
				.and(MemberSpecification.withIsApproved(isApproved))
				.and(MemberSpecification.withMiddleName(middleName))
				.and(MemberSpecification.withEmail(email))
				.and(MemberSpecification.withCreatedDateBetween(startDate, endDate)),pageable);
	}

	@Override
	public Member create(Member member) throws Exception {
		memberRepository.save(member);
		return member;
	}

	@Override
	public Member findById(int id) throws Exception {
		return memberRepository.findById(id);
	}

	@Override
	public Member update(Member member) throws Exception {
		memberRepository.save(member);
		return member;
	}

	@Override
	public Member delete(int id) throws Exception {
		Member user = findById(id);
		if (user!=null)
			memberRepository.delete(user);
		return user;
	}

	@Override
	public List<Member> update(List<MemberData> userRfid) throws Exception {
		List<Member> users = new ArrayList<Member>();
		logger.info("Updating Member Details");
		for(MemberData rfid:userRfid) {
			if(rfid.getRfid() !=null && !rfid.getRfid().isEmpty()) {
				
				List<Member> rfidUser = memberRepository.findByPrcLicenseNumber(rfid.getPrcNumber());
				if(rfidUser!=null && rfidUser.size() > 0) {
					for(Member user:rfidUser) {
						logger.info("Update Member:" + user.getId() + " rfid to:" + rfid.getRfid());
						user.setRfid(rfid.getRfid());
						memberRepository.save(user);
						users.add(user);
					}
				}
			}else {
				List<Member> rfidUser = memberRepository.findByLastNameAndFirstNameIgnoreCase(rfid.getLastName(), rfid.getFirstName());
				if(rfidUser!=null && rfidUser.size() > 0) {
					for(Member user:rfidUser) {
						logger.info("Update Member:" + user.getId() + " PRC to:" + rfid.getPrcNumber());
						user.setPrcLicenseNumber(rfid.getPrcNumber());
						if(user.getMiddleName() == null || user.getMiddleName().isEmpty())
							user.setMiddleName(rfid.getMiddleName());
						memberRepository.save(user);
						users.add(user);
					}
				}
			}
				
		}
		return users;
	}

	@Override
	public Member update(int id, String rfid) throws Exception {
		Member user = memberRepository.findById(id);
		if(user != null) {
			user.setRfid(rfid);
			memberRepository.save(user);
		}
		return user;
	}

	@Override
	public List<UserExport> download(Integer chapterId, String firstName, String lastName, String skills,
			String industries, String prcLicenseNumber, String yearObtained, Integer isApproved) throws Exception {
		return userExportRepository.findAll(Specification.where(UserExportSpecification.withChapterId(chapterId))
				.and(UserExportSpecification.withFirstName(firstName))
				.and(UserExportSpecification.withIndustries(industries))
				.and(UserExportSpecification.withLastName(lastName))
				.and(UserExportSpecification.withPrcLicenseNumber(prcLicenseNumber))
				.and(UserExportSpecification.withSkills(skills))
				.and(UserExportSpecification.withYearObtained(yearObtained))
				.and(UserExportSpecification.withIsApproved(isApproved)));
	}
}
