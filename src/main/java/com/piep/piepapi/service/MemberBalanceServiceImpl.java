package com.piep.piepapi.service;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalanceHeader;
import com.piep.piepapi.domain.MemberBalancePayment;
import com.piep.piepapi.domain.MemberBalancePaymentHeader;
import com.piep.piepapi.domain.User;
import com.piep.piepapi.email.model.EmailMessage;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.email.model.MemberDues;
import com.piep.piepapi.email.model.TemplateParams;
import com.piep.piepapi.email.model.TemplateResult;
import com.piep.piepapi.email.service.EmailService;
import com.piep.piepapi.email.service.TemplateEngine;
import com.piep.piepapi.enumeration.PaymentStatus;
import com.piep.piepapi.repository.MemberBalanceHeaderRepository;
import com.piep.piepapi.repository.MemberBalancePaymentHeaderRepository;
import com.piep.piepapi.repository.MemberBalancePaymentRepository;
import com.piep.piepapi.repository.MemberBalanceRepository;
import com.piep.piepapi.repository.MemberRepository;
import com.piep.piepapi.repository.UserRepository;
import com.piep.piepapi.specification.MemberBalanceSpecification;

@Service
public class MemberBalanceServiceImpl implements MemberBalanceService {

	@Autowired
	MemberBalanceRepository memberBalanceRepository;

	@Autowired
	MemberBalanceHeaderRepository memberBalanceHeaderRepository;
	
	@Autowired
	private TemplateEngine templateEngine;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	MemberRepository memberRepository;
	
	@Autowired
	MemberBalancePaymentRepository memberBalancePaymentRepository;

	
	@Override
	public Page<MemberBalance> findAll(Integer userId,Integer chapterId, String description, Integer isPaid,Integer isMembership, Date startDate, Date endDate,
			Double startAmount, Double endAmount, Pageable pageable) throws Exception {

		return memberBalanceRepository.findAll(Specification.where(MemberBalanceSpecification.withUserId(userId))
				.and(MemberBalanceSpecification.withDescription(description))
				.and(MemberBalanceSpecification.withIsPaid(isPaid))
				.and(MemberBalanceSpecification.withCreatedDateBetween(startDate, endDate))
				.and(MemberBalanceSpecification.withAmountBetween(startAmount, endAmount))
				.and(MemberBalanceSpecification.joinUserChaptedId(chapterId))
				.and(MemberBalanceSpecification.withIsMembership(isMembership)), pageable);

	}

	@Override
	public Page<MemberBalanceHeader> create(List<MemberBalanceHeader> memberBalance, Pageable pageable) throws Exception {
		 List<MemberBalanceHeader> memberBalanceHeaders = new ArrayList<MemberBalanceHeader>();
		for(MemberBalanceHeader memberBalanceHeader:memberBalance) {
			if (memberBalanceHeader.getUserId() == null)
				throw new Exception("Missing User ID");
			memberBalanceHeader.setCreatedDate(new Date());
			memberBalanceHeaders.add(memberBalanceHeader);
		}
		memberBalanceHeaderRepository.saveAll(memberBalanceHeaders);
		
		long totalRows = 0;
		if (memberBalanceHeaders != null && memberBalanceHeaders.size() > 0)
			totalRows = memberBalanceHeaders.size();

		
		Page<MemberBalanceHeader> memberBalanceHeadersPage = new PageImpl(memberBalanceHeaders, pageable, totalRows);

		return memberBalanceHeadersPage;
	}

	@Override
	public MemberBalance findById(int id) throws Exception {
		MemberBalance memberBalance = memberBalanceRepository.findById(id);
		return memberBalance;
	}

	@Override
	public MemberBalanceHeader update(MemberBalanceHeader memberBalance) throws Exception {
		memberBalance.setUpdateDate(new Date());
		memberBalanceHeaderRepository.save(memberBalance);
		return memberBalance;
	}

	@Override
	public MemberBalanceHeader delete(int id) throws Exception {
		MemberBalanceHeader memberBalance = memberBalanceHeaderRepository.findById(id);
		memberBalanceHeaderRepository.delete(memberBalance);
		return memberBalance;
	}
	
	@Override
	public GeneralResponse sendManualEmailPayment(int paymentId) {
		GeneralResponse generalResponse = new GeneralResponse();
		try{
			List<MemberBalance> memeberBalances = memberBalanceRepository.findByMemberBalancePaymentPaymentId(paymentId) ;
			if(memeberBalances !=null && memeberBalances.size() > 0) {
				
				String paymentDescription =  memeberBalances.get(0).getMemberBalancePayment().get(0).getPayment().getDescription();
				if(paymentDescription.equals(null)){
					generalResponse.setMessage("Email was not sent, payment description is null");
					generalResponse.setResponse("ERROR");
					return generalResponse;
				}
				
				TemplateParams templateParams = new TemplateParams();
				String currency =  memeberBalances.get(0).getMemberBalancePayment().get(0).getPayment().getCurrency();
				List<String> descriptions = new ArrayList<String>();
				for(MemberBalance memberBalance:memeberBalances) {
					/*String description = String.format("%s: %s %s", memberBalance.getDescription(), currency,
							memberBalance.getAmount().toString());*/
					String description = memberBalance.getDescription();
					descriptions.add(description);
				}
				templateParams.addParam("descriptions", descriptions);
				templateParams.addParam("paymentDescriptions",paymentDescription);
				templateParams.addParam("fullName", memeberBalances.get(0).getUser().getFullname());
				templateParams.addParam("total", memeberBalances.get(0).getMemberBalancePayment().get(0).getPayment().getAmount());
				templateParams.addParam("paymentUrl", "http://app.piep.org.ph/membership/payment/" + paymentId + "/");
				templateParams.addParam("currency",currency);
				
				TemplateResult templateResult = new TemplateResult();
				templateResult = templateEngine.process("manual_payment", templateParams);
				EmailMessage emailMsg = new EmailMessage();
				emailMsg.setFromEmail("support@piep.org.ph");
			    emailMsg.setSubject(templateResult.getSubject());
				emailMsg.setBody(templateResult.getBody());
				List<String> toEmails = new ArrayList<String>();
				toEmails.add(memeberBalances.get(0).getUser().getEmail());
				//toEmails.add("crisandro_prado@yahoo.com");
				emailMsg.setToEmails(toEmails);
				emailService.send(emailMsg);
				generalResponse.setMessage("Email was sent to:" + memeberBalances.get(0).getUser().getEmail());
				generalResponse.setResponse("OK");
			}
			else {
				generalResponse.setMessage("Email was not sent, payment id " + paymentId + " not found on Member Balance");
				generalResponse.setResponse("ERROR");
			}
		}
		catch(Exception ex) {
			generalResponse.setMessage(ex.getMessage());
			generalResponse.setResponse("ERROR");
		}
		return generalResponse;
	}

	@Override
	public List<MemberBalance> findByMemberBalancePaymentPaymentId(int paymentId) {
		return memberBalanceRepository.findByMemberBalancePaymentPaymentId(paymentId) ;
	}

	@Override
	public Page<MemberBalanceHeader> upload(List<MemberDues> memberBalance, Pageable pageable) throws Exception {
		List<MemberBalanceHeader> memberBalanceHeaders =  new ArrayList<MemberBalanceHeader>();
		for(MemberDues memberDues:memberBalance) {
			List<User> users = userRepository.findByLastNameAndFirstNameIgnoreCase(memberDues.getLastName(), memberDues.getFirstName());
			if(users != null && users.size() > 0) {
				for(User user:users) {
					MemberBalanceHeader memberBalanceHeader = new MemberBalanceHeader();
					memberBalanceHeader.setUserId(user.getId());
					memberBalanceHeader.setAmount(memberDues.getAmount());
					memberBalanceHeader.setDescription(memberDues.getDescription());
					memberBalanceHeader.setCreatedBy("system");
					memberBalanceHeader.setCreatedDate(new Date());
					memberBalanceHeaders.add(memberBalanceHeader);
				}
			}
		}
		
		Page<MemberBalanceHeader> memberBalanceHeadersPage  = null;
		
		if(memberBalanceHeaders !=null && memberBalanceHeaders.size() > 0) {
			memberBalanceHeaderRepository.saveAll(memberBalanceHeaders);
			
			long totalRows = 0;
			if (memberBalanceHeaders != null && memberBalanceHeaders.size() > 0)
				totalRows = memberBalanceHeaders.size();
			 memberBalanceHeadersPage = new PageImpl(memberBalanceHeaders, pageable, totalRows);
		}
		
		return memberBalanceHeadersPage;
	}

	@Override
	public MemberBalanceHeader update(int paymendId) throws Exception {
		
		MemberBalancePayment memberBalancePayment = memberBalancePaymentRepository.findByPaymentId(paymendId);
		
		MemberBalanceHeader memberBalanceHeader = new MemberBalanceHeader();
		if(memberBalancePayment!=null) {
			memberBalanceHeader =  memberBalanceHeaderRepository.findById(memberBalancePayment.getBalanceId());
			if(memberBalanceHeader!=null) {
				
				List<MemberBalancePayment> memberBalancePayments = memberBalancePaymentRepository
						.findByBalanceId(memberBalancePayment.getBalanceId());
				//Double amountPaid = memberBalancePayment.getAmount();
				Integer isPaid = 1;
				if (memberBalancePayments != null) {
					for (MemberBalancePayment existingMemberBalancePayment : memberBalancePayments) {
						if(existingMemberBalancePayment.getPayment() != null) {
							if(!existingMemberBalancePayment.getPayment().getStatus().equalsIgnoreCase(PaymentStatus.success.getValue())) {
								isPaid = 0;
								break;
							}	
						}
					}
				}
				else
					isPaid = 0;
				
				memberBalanceHeader.setIsPaid(isPaid);
				memberBalanceHeaderRepository.save(memberBalanceHeader);
			}
		}
		
		return memberBalanceHeader;
	}

	@Override
	public Page<MemberBalanceHeader> membership(String year, Double amount, Pageable pageable) throws Exception {
		
		List<MemberBalanceHeader> memberBalanceHeaders =  new ArrayList<MemberBalanceHeader>();
		LocalDate now = LocalDate.now();
		LocalDate firstDay = now.with(firstDayOfYear());
		Date firstDate = new SimpleDateFormat("yyyy-MM-dd").parse(firstDay.toString());
		List<Member> members = memberRepository.findByDateJoinedLessThan(firstDate);
		if(members != null && members.size() > 0) {
			for(Member member:members) {
				MemberBalanceHeader memberBalanceHeader = new MemberBalanceHeader();
				memberBalanceHeader.setUserId(member.getId());
				memberBalanceHeader.setAmount(amount);
				memberBalanceHeader.setDescription("CY-" + year);
				memberBalanceHeader.setCreatedBy("system");
				memberBalanceHeader.setCreatedDate(new Date());
				memberBalanceHeaders.add(memberBalanceHeader);
			}
		}
		
		Page<MemberBalanceHeader> memberBalanceHeadersPage  = null;
		
		if(memberBalanceHeaders !=null && memberBalanceHeaders.size() > 0) {
			memberBalanceHeaderRepository.saveAll(memberBalanceHeaders);
			
			long totalRows = 0;
			if (memberBalanceHeaders != null && memberBalanceHeaders.size() > 0)
				totalRows = memberBalanceHeaders.size();
			 memberBalanceHeadersPage = new PageImpl(memberBalanceHeaders, pageable, totalRows);
		}
		
		return memberBalanceHeadersPage;
	}

}
