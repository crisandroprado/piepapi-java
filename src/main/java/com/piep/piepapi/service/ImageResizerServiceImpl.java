package com.piep.piepapi.service;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.piep.piepapi.util.JpegReader;
import com.piep.piepapi.util.URLEncoding;


@Service
public class ImageResizerServiceImpl implements ImageResizerService {

	@Autowired
	private URLEncoding uRLEncoding;
	
	@Autowired 
	private JpegReader jpegReader;
	
	@Override
	// @Cacheable("resizeImage")
	public Resource resizeImage(String imageUrl, String format, Integer width, Integer height, Double percentage)
			throws Exception {

		ByteArrayResource resource = null;
		RandomAccessBufferedFileInputStream inputStream = null;
		ByteArrayOutputStream baos = null;
		try {
			
			/*CacheManager cm = CacheManager.newInstance();
			Cache cache = cm.getCache("image");
			
			byte[] imageBytesCache = null;
			if (cache.isKeyInCache(imageUrl)) {
				Element element = cache.get(imageUrl);
				cache.remo
				imageBytesCache = (byte[]) element.getObjectValue();
			}
			if (imageBytesCache != null) {
				ByteArrayResource cacheResource = null;
				cacheResource = new ByteArrayResource(imageBytesCache);
				return cacheResource;
			}*/

			// BufferedImage image = ImageIO.read(inputStream);
			BufferedImage image = null;
			URL url = new URL(uRLEncoding.encode(imageUrl));
			inputStream = new RandomAccessBufferedFileInputStream(url.openStream());
			try {
				image = ImageIO.read(inputStream);
			} catch (IllegalArgumentException ia) {
				image = jpegReader.readImage(inputStream, format);
			} 
			int scaledWidth = image.getWidth();
			int scaledHeight = image.getHeight();
			if (width == null && height == null && percentage != null) {
				double percent = (double) (percentage / 100.00);
				scaledWidth = (int) (image.getWidth() * percent);
				scaledHeight = (int) (image.getHeight() * percent);
			} else if (width != null && height == null) {
				double widthDouble = (double) width;
				double percent = widthDouble / scaledWidth;
				scaledWidth = (int) (image.getWidth() * percent);
				scaledHeight = (int) (image.getHeight() * percent);

			} else if (width == null && height != null) {
				double heightDouble = (double) height;
				double percent = heightDouble / scaledHeight;
				scaledWidth = (int) (image.getWidth() * percent);
				scaledHeight = (int) (image.getHeight() * percent);
			} else {
				scaledWidth = width;
				scaledHeight = height;
			}
			Image tmp = image.getScaledInstance(scaledWidth, scaledHeight, Image.SCALE_SMOOTH);
			BufferedImage resized = new BufferedImage(tmp.getWidth(null), tmp.getHeight(null),
					BufferedImage.TYPE_INT_RGB);
			resized.getGraphics().drawImage(tmp, 0, 0, null);

			baos = new ByteArrayOutputStream();
			ImageIO.write(resized, format, baos);
			baos.flush();
			byte[] imageBytes = baos.toByteArray();
			resource = new ByteArrayResource(imageBytes);

			//cache.put(new Element(imageUrl, imageBytes));
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			if (inputStream != null)
				inputStream.close();
			if (baos != null)
				baos.close();
		}

		return resource;
	}
}
