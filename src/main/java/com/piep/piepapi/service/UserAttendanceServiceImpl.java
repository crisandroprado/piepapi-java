package com.piep.piepapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.piep.piepapi.domain.UserAttendance;
import com.piep.piepapi.repository.UserAttendanceRepository;
import com.piep.piepapi.specification.UserAttendanceSpecification;

@Service
public class UserAttendanceServiceImpl implements UserAttendanceService{

	@Autowired
	UserAttendanceRepository userAttendanceRepository;
	
	@Override
	public Page<UserAttendance> findAll(String dateIn, String dateOut,Integer eventId, Pageable pageable) throws Exception {
		return userAttendanceRepository
				.findAll(Specification.where(UserAttendanceSpecification.withDateIn(dateIn))
						.and(UserAttendanceSpecification.withDateOut(dateOut))
						.and(UserAttendanceSpecification.withEventId(eventId)), pageable);

	}

	@Override
	public List<UserAttendance> create(List<UserAttendance> userAttendance) throws Exception {
		userAttendanceRepository.saveAll(userAttendance);
		return userAttendance;
	};

	@Override
	public Page<UserAttendance> findByRfid(String rfid,Pageable pageable) throws Exception {
		Page<UserAttendance> userAttendance = userAttendanceRepository.findByRfid(rfid,pageable);
		return userAttendance;
	}

	@Override
	public List<UserAttendance> delete(String rfid) throws Exception {
		List<UserAttendance> userAttendance = userAttendanceRepository.findByRfid(rfid);
		if(userAttendance!=null)
			userAttendanceRepository.deleteAll(userAttendance);
		return userAttendance;
	}

	@Override
	public Page<UserAttendance> findByUserId(int id,Pageable pageable) throws Exception {
		Page<UserAttendance> userAttendance = userAttendanceRepository.findByUserId(id,pageable);
		return userAttendance;
	}

	
}
