package com.piep.piepapi.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.Membership;
import com.piep.piepapi.email.model.MemberData;

public interface MembershipService {

	Page<Membership> findAll(Integer startYear,Integer endYear, String firstName, String lastName, String status,Date startDate,Date endDate, Pageable pageable) throws Exception;

	Page<Membership> createAll(List<Membership> memberships,Pageable pageable) throws Exception;

	Membership findById(int id) throws Exception;

	Membership update(Membership memberships) throws Exception;

	Membership delete(int id) throws Exception;
	
	Membership create(Membership memberships) throws Exception;
}
