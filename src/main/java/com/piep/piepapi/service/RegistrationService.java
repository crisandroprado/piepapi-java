package com.piep.piepapi.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.Registration;

public interface RegistrationService {

	Page<Registration> findAll(int eventId, Pageable pageable) throws Exception;

	Registration create(Registration registration) throws Exception;

	Registration findById(int eventId, int id) throws Exception;

	Registration update(Registration registration) throws Exception;

	Registration delete(int eventId, int id) throws Exception;
	
	Registration resendTicket(int eventId, int id) throws Exception;
}
