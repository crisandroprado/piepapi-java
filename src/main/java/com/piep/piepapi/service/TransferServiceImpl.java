package com.piep.piepapi.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.piep.piepapi.configuration.Config;
import com.piep.piepapi.email.model.SocialMediaBanner;
import org.apache.commons.codec.binary.Base64;

@Service
public class TransferServiceImpl implements TransferService {

	private static Logger logger = LoggerFactory.getLogger(TransferServiceImpl.class);

	@Autowired
	Config config;
	
	@Override
	public void upload(SocialMediaBanner socialMediaBanner) throws Exception {
		
		Path path = Paths.get(config.getSocialMediaBannerDir() + socialMediaBanner.getFileName());

		byte[] data = Base64.decodeBase64(socialMediaBanner.getFileAsBase64String());
		Files.write(path, data, StandardOpenOption.CREATE);
	}

	@Override
	public byte[] download(String filename) throws Exception {
		
		Path path = Paths.get(config.getSocialMediaBannerDir() + filename);
		return Files.readAllBytes(path);
	}

}
