package com.piep.piepapi.service;

import org.springframework.core.io.Resource;

public interface ImageResizerService {

	Resource resizeImage(String imageUrl, String format, Integer width, Integer height, Double percentage) throws Exception;
}
