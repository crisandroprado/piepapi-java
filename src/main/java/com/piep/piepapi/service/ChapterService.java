package com.piep.piepapi.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.Chapter;

public interface ChapterService {

	Page<Chapter> findAll(Pageable pageable) throws Exception;

	Chapter create(Chapter chapter) throws Exception;

	Chapter findById(int id) throws Exception;

	Chapter update(Chapter chapter) throws Exception;

	Chapter delete(int id) throws Exception;
}
