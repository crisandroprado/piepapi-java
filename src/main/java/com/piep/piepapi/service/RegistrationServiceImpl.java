package com.piep.piepapi.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.piep.piepapi.domain.Registration;
import com.piep.piepapi.email.model.EmailMessage;
import com.piep.piepapi.email.model.TemplateParams;
import com.piep.piepapi.email.model.TemplateResult;
import com.piep.piepapi.email.service.EmailService;
import com.piep.piepapi.email.service.TemplateEngine;
import com.piep.piepapi.repository.RegistrationRepository;

@Service
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	RegistrationRepository registrationRepository;
	
	@Autowired
	private TemplateEngine templateEngine;
	
	@Autowired
	private EmailService emailService;
	

	@Override
	public Page<Registration> findAll(int eventId, Pageable pageable) throws Exception {
		return registrationRepository.findByEventId(eventId, pageable);
	}

	@Override
	public Registration create(Registration registration) throws Exception {
		
		//registration.setFee(registration.getEvent().getAmount());
		
		registration.getPayment().setTransactionNumber(RandomStringUtils.randomAlphanumeric(20));
		registration.getPayment().setReferenceNumber(RandomStringUtils.randomAlphanumeric(8).toUpperCase());
		registration.getPayment().setToken(RandomStringUtils.randomAlphanumeric(8).toUpperCase());
		
		registrationRepository.save(registration);
		
		TemplateParams templateParams = new TemplateParams();
		templateParams.addParam("event", registration.getEvent());
		templateParams.addParam("registration", registration);
		
		TemplateResult templateResult = templateEngine.process("event_registration_email", templateParams);
		EmailMessage emailMsg = new EmailMessage();
		emailMsg.setFromEmail("support@piep.org.ph");
	    emailMsg.setSubject(templateResult.getSubject());
		emailMsg.setBody(templateResult.getBody());
		List<String> toEmails = new ArrayList<String>();
		//toEmails.add(registration.getToEmail());
		toEmails.add("");
		
		emailMsg.setToEmails(toEmails);
		
		emailService.send(emailMsg);
		
		return registration;
	}

	@Override
	public Registration findById(int eventId, int id) throws Exception {
		return registrationRepository.findByEventIdAndId(eventId, id);
		
	}

	@Override
	public Registration update(Registration registration) throws Exception {
		registrationRepository.save(registration);
		return registration;
	}

	@Override
	public Registration delete(int eventId, int id) throws Exception {
		Registration registration = findById(eventId,id);
		if(registration!=null)
			registrationRepository.delete(registration);
		return registration;
	}

	@Override
	public Registration resendTicket(int eventId, int id) throws Exception {
		Registration registration = findById(eventId,id);
		return registration;
		/*if(registration.getPayment().getIsPaid()) {
			TemplateParams templateParams = new TemplateParams();
			templateParams.addParam("event", registration.getEvent());
			templateParams.addParam("registration", registration);
			templateParams.addParam("user", registration.getEvent().getUser());
			templateParams.addParam("payment", registration.getPayment());

			TemplateResult templateResult = new TemplateResult();
			if(registration.getIsAdditional() == 0)
				templateEngine.process("event_payment", templateParams);
			else
				templateEngine.process("event_ticket_additional", templateParams);
			EmailMessage emailMsg = new EmailMessage();
			emailMsg.setFromEmail("support@piep.org.ph");
		    emailMsg.setSubject(templateResult.getSubject());
			emailMsg.setBody(templateResult.getBody());
			List<String> toEmails = new ArrayList<String>();
			toEmails.add(registration.getToEmail());
			emailMsg.setToEmails(toEmails);
			emailService.send(emailMsg);
			return registration;
		}
		else
			throw new Exception("Ticket not paid yet.");*/
		
	}
	
	
}
