package com.piep.piepapi.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.Payment;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.repository.PaymentRepository;
import com.piep.piepapi.specification.PaymentSpecification;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	PaymentRepository paymentRepository;

	@Override
	public Page<Payment> findAll(Integer userId, String type, String transactionNumber, String token,
			String referenceNumber, String status,String method, Double amount, Date createdAt, Date updatedAt, Pageable pageable)
			throws Exception {
		return paymentRepository.findAll(Specification.where(PaymentSpecification.withAmount(amount))
				.and(PaymentSpecification.withCreatedAt(createdAt))
				.and(PaymentSpecification.withReferenceNumber(referenceNumber))
				.and(PaymentSpecification.withStatus(status)).and(PaymentSpecification.withToken(token))
				.and(PaymentSpecification.withTransactionNumber(transactionNumber))
				.and(PaymentSpecification.withType(type)).and(PaymentSpecification.withUpdatedAt(updatedAt))
				.and(PaymentSpecification.withMethod(method)).and(PaymentSpecification.withUserId(userId)), pageable);

	}

	@Override
	public Payment findById(int id) throws Exception {
		return paymentRepository.findById(id);
	}

	

}
