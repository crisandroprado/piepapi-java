package com.piep.piepapi.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.Payment;
import com.piep.piepapi.email.model.GeneralResponse;

public interface PaymentService {

	Page<Payment> findAll(Integer userId, String type, String transactionNumber, String token, String referenceNumber,
			String status,String method, Double amount, Date createdAt, Date updatedAt, Pageable pageable) throws Exception;


	Payment findById(int id) throws Exception;

}
