package com.piep.piepapi.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.piep.piepapi.domain.EventsInvitation;
import com.piep.piepapi.email.model.GeneralResponse;

public interface EventsInvitationService {

	Page<EventsInvitation> findAll(Integer eventId, Integer userId,Pageable pageable) throws Exception;

	EventsInvitation create(EventsInvitation eventsInvitation) throws Exception;

	EventsInvitation findById(int id) throws Exception;

	EventsInvitation update(EventsInvitation eventsInvitation) throws Exception;

	EventsInvitation delete(int id) throws Exception;
	
	GeneralResponse sendEventsInvitationEmail(int id);
}
