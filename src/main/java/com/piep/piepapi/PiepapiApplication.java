package com.piep.piepapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiepapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PiepapiApplication.class, args);
	}

}

