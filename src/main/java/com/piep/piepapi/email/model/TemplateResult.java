package com.piep.piepapi.email.model;

import lombok.Data;

@Data
public class TemplateResult {
	
	private String subject;
	private String body;
}
