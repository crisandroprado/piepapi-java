package com.piep.piepapi.email.model;

import java.util.HashMap;

public class TemplateParams {

	private HashMap<String,Object> params = new HashMap<String,Object>();
	
	public TemplateParams addParam(String prop, Object value) {
		params.put(prop, value);
		return this;
	}
	
	public HashMap<String,Object> toMap() {
		return params;
	}
}
