package com.piep.piepapi.email.model;

import lombok.Data;

@Data
public class SocialMediaBanner {

	 private String fileAsBase64String;
	 private String fileName;
}
