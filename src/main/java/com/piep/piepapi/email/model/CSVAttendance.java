package com.piep.piepapi.email.model;

import com.opencsv.bean.CsvBindByName;

public class CSVAttendance {

	 @CsvBindByName(column = "EPC", required = true)
	 private String rfid;
	 
	 @CsvBindByName(column = "Count", required = true)
	 private String count;
	 
	 @CsvBindByName(column = "Last seen time", required = true)
	 private String timeOut;
	 
	 @CsvBindByName(column = "Last seen date", required = true)
	 private String dateOut;
	 
	 @CsvBindByName(column = "First seen time", required = true)
	 private String timeIn;
	 
	 @CsvBindByName(column = "First seen date", required = true)
	 private String dateIn;

}
