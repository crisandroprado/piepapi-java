package com.piep.piepapi.email.model;

import lombok.Data;

@Data
public class MemberDues {

	private String description;
	private String firstName;
	private String lastName;
	private Double amount;
}
