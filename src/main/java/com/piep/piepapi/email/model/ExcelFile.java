package com.piep.piepapi.email.model;

import lombok.Data;

@Data
public class ExcelFile {

	 private String fileAsBase64String;
	 private String sheetName;
	 private Integer firstColumnIndex;
	 private Integer lastColumnIndex;
	 private String cellNameDescription;
}
