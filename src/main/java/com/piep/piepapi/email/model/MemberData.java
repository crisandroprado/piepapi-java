package com.piep.piepapi.email.model;

import lombok.Data;

@Data
public class MemberData {

	private String rfid;
	private String fullName;
	private String prcNumber;
	private String lastName;
	private String firstName;
	private String middleName;
}
