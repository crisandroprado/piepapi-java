package com.piep.piepapi.email.model;

import lombok.Data;

@Data
public class EmailAttachment {

	private String name;
	private byte[] content = new byte[0];
}
