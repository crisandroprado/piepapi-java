package com.piep.piepapi.email.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class EmailMessage {

	private String fromEmail;
	private String fromName;
	private List<String> toEmails = new ArrayList<String>();
	private List<String> ccEmails = new ArrayList<String>();
	private List<String> bccEmails = new ArrayList<String>();
	private String subject;
	private String body;
	private boolean highImportance;
	private List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
}
