package com.piep.piepapi.email.model;

import lombok.Data;

@Data
public class GeneralResponse {

	private String response;
	private String message;
}
