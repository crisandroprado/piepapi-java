package com.piep.piepapi.email.service;

import com.piep.piepapi.email.model.EmailMessage;

public interface EmailService {

	void send(EmailMessage emailMsg) throws Exception ;
}
