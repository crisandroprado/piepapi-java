package com.piep.piepapi.email.service;

import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.piep.piepapi.configuration.Config;
import com.piep.piepapi.email.model.EmailAttachment;
import com.piep.piepapi.email.model.EmailMessage;

@Service
public class EmailServiceImpl implements EmailService {

	private static Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Autowired
	Config config;

	@Override
	public void send(EmailMessage emailMsg) throws Exception {
		HtmlEmail email = new HtmlEmail();
		try {
			email.setHostName(config.getMailHost());
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(config.getMailUsername(), config.getMailPassword());
				}
			};
			email.setAuthenticator(auth);
			email.setSmtpPort(Integer.parseInt(config.getMailPort()));
			email.setCharset("UTF-8");
			email.setFrom(emailMsg.getFromEmail(), emailMsg.getFromName());

			// setting the connection timeout in milliseconds
			// email.setSocketConnectionTimeout(Integer.parseInt(config.getMailTimeout()));
			// email.setSocketTimeout(Integer.parseInt(config.getMailTimeout()));

			for (String toEmail : emailMsg.getToEmails()) {
				email.addTo(toEmail);
			}
			for (String ccEmail : emailMsg.getCcEmails()) {
				email.addCc(ccEmail);
			}
			for (String bccEmail : emailMsg.getBccEmails()) {
				email.addBcc(bccEmail);
			}
			email.setSubject(emailMsg.getSubject());

			email.setHtmlMsg(emailMsg.getBody());
			if (emailMsg.getAttachments() != null) {
				for (EmailAttachment attachment : emailMsg.getAttachments()) {
					DataSource ds = new ByteArrayDataSource(attachment.getContent(), "application/octet-stream");
					email.attach(ds, attachment.getName(), attachment.getName(),
							org.apache.commons.mail.EmailAttachment.ATTACHMENT);
					logger.info("Email has attachment:" + attachment.getName());
				}
			}

			logger.info("Sending via piep smtp");
			email.send();
		} catch (EmailException e) {
			Throwable cause = e.getCause();
			if (cause != null && cause instanceof SendFailedException) {
				throw (SendFailedException) cause;
			} else {
				throw new RuntimeException(e);
			}
		}

	}

}
