package com.piep.piepapi.email.service;

import java.io.IOException;
import java.io.StringWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.piep.piepapi.email.model.TemplateParams;
import com.piep.piepapi.email.model.TemplateResult;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class TemplateEngineImpl implements TemplateEngine {

	@Autowired
	private Configuration config;
	
	@Override
	public void TemplateEngine() {
		//config.setClassForTemplateLoading(TemplateEngine.class, "/com/piep/piepapi/email/templates");
		config.setClassForTemplateLoading(TemplateEngine.class, "/templates/");
		
	}

	@Override
	public TemplateResult process(String name, TemplateParams params) {
		String subject = processTemplate(name + ".subject", params);
		String body = processTemplate(name + ".body", params);
		TemplateResult templateResult =  new TemplateResult();
		templateResult.setSubject(subject);
		templateResult.setBody(body);
		return templateResult;
	}
	
	private String processTemplate(String name, TemplateParams params) {
		StringWriter out = new StringWriter();
		try {
			Template template = config.getTemplate(name);
			template.process(params.toMap(), out);
			return out.toString();
		}
		catch(IOException e) {
			throw new RuntimeException(e);
		}
		catch(TemplateException e) {
			throw new RuntimeException(e);
		}
	}

}
