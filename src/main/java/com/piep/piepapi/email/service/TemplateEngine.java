package com.piep.piepapi.email.service;

import com.piep.piepapi.email.model.TemplateParams;
import com.piep.piepapi.email.model.TemplateResult;

public interface TemplateEngine {

	void TemplateEngine();
	
	TemplateResult process(String name, TemplateParams params);
}
