package com.piep.piepapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.MemberBalancePayment;
import com.piep.piepapi.domain.MemberBalancePaymentHeader;

@Repository
public interface MemberBalancePaymentHeaderRepository
		extends JpaRepository<MemberBalancePaymentHeader, Integer>, JpaSpecificationExecutor<MemberBalancePayment> {

	MemberBalancePaymentHeader findById(int id);

	List<MemberBalancePaymentHeader> findByBalanceId(int balanceId);
}
