package com.piep.piepapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalanceHeader;

@Repository
public interface MemberBalanceHeaderRepository
		extends JpaRepository<MemberBalanceHeader, Integer>, JpaSpecificationExecutor<MemberBalance> {

	MemberBalanceHeader findById(int id);
}
