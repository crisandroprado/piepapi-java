package com.piep.piepapi.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.UserAttendance;

@Repository
public interface UserAttendanceRepository
		extends JpaRepository<UserAttendance, Integer>, JpaSpecificationExecutor<UserAttendance> {

	Page<UserAttendance> findByRfid(String rfid, Pageable pageable);

	Page<UserAttendance> findByUserId(int id, Pageable pageable);
	
	List<UserAttendance> findByRfid(String rfid);
}
