package com.piep.piepapi.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {

	Event findById(int id);
	
	Page<Event> findByEndDateGreaterThan(@DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate, Pageable pageable);
	
	Page<Event> findByEndDateLessThan(@DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate, Pageable pageable);
}
