package com.piep.piepapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.Payment;

@Repository
public interface PaymentRepository
		extends JpaRepository<Payment, Integer>, JpaSpecificationExecutor<Payment> {

	Payment findById(int id);
}
