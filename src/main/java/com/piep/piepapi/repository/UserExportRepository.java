package com.piep.piepapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.UserExport;

@Repository
public interface UserExportRepository extends JpaRepository<UserExport, Integer>, JpaSpecificationExecutor<UserExport> {

}
