package com.piep.piepapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.Membership;

@Repository
public interface MembershipRepository extends JpaRepository<Membership, Integer>, JpaSpecificationExecutor<Membership> {

	Membership findById(int id);
}
