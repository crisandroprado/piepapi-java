package com.piep.piepapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalancePayment;

@Repository
public interface MemberBalancePaymentRepository
		extends JpaRepository<MemberBalancePayment, Integer>, JpaSpecificationExecutor<MemberBalancePayment> {

	MemberBalancePayment findById(int id);
	
	List<MemberBalancePayment> findByBalanceId(int balanceId);
	
	MemberBalancePayment findByPaymentId(int balanceId);
	
}
