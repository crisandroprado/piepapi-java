package com.piep.piepapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Integer> {

	Registration findByEventIdAndId(int eventId, int id);

	Page<Registration> findByEventId(int eventId, Pageable pageable);
}
