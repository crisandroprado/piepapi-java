package com.piep.piepapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.EventsInvitation;

@Repository
public interface EventsInvitationRepository
		extends JpaRepository<EventsInvitation, Integer>, JpaSpecificationExecutor<EventsInvitation> {

	EventsInvitation findById(int id);
}
