package com.piep.piepapi.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.User;

@Repository
public interface MemberRepository extends JpaRepository<Member, Integer>, JpaSpecificationExecutor<Member> {

	Member findById(int id);

	List<Member> findByPrcLicenseNumber(String prc);
	
	List<Member> findByLastNameAndFirstNameIgnoreCase(String lastName, String firstName);
	
	List<Member> findByDateJoinedLessThan(@DateTimeFormat(pattern = "yyyy-MM-dd") Date dateJoined);
}
