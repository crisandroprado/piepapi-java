package com.piep.piepapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	// User findById(int id);

	// List<User> findByPrcLicenseNumber(String prc);
	
	List<User> findAll();
	
	List<User> findByLastNameAndFirstNameIgnoreCase(String lastName, String firstName);
}
