package com.piep.piepapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.piep.piepapi.domain.MemberBalance;

@Repository
public interface MemberBalanceRepository
		extends JpaRepository<MemberBalance, Integer>, JpaSpecificationExecutor<MemberBalance> {

	MemberBalance findById(int id);
	
	List<MemberBalance> findByMemberBalancePaymentPaymentId(int paymentId) ;
}
