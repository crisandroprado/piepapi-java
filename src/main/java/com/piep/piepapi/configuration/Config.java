package com.piep.piepapi.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@Data
public class Config {
	
	@Value("${mail.host}")
	private String mailHost;
	
	@Value("${mail.port}")
	private String mailPort;
	
	@Value("${mail.timeout}")
	private String mailTimeout;
	
	@Value("${mail.username}")
	private String mailUsername;
	
	@Value("${mail.password}")
	private String mailPassword;
	
	@Value("${secret.key}")
	private String secretKey;
	
	@Value("${dragonpay.test.mode}")
	private String dragonpayTestMode;
	
	@Value("${dragonpay.id}")
	private String dragonpayId;
	
	@Value("${dragonpay.password}")
	private String dragonpayPassword;
	
	@Value("${dragonpay.api.key}")
	private String dragonpayApiKey;
	
	@Value("${dragonpay.prod.url}")
	private String dragonpayProdUrl;
	
	@Value("${dragonpay.payout.prod.url}")
	private String dragonpayPayoutProdUrl;
	
	@Value("${dragonpay.test.url}")
	private String dragonpayTestUrl;
	
	@Value("${dragonpay.payout.test.url}")
	private String dragonpayPayoutTestUrl;
	
	@Value("${dragonpay.pay.url}")
	private String dragonpayPayUrl;
	
	@Value("${dragonpay.merchant.url}")
	private String dragonpayMerchantUrl;
	
	@Value("${dragonpay.soap.url}")
	private String dragonpaySoapUrl;
	
	@Value("${dragonpay.payout.url}")
	private String dragonpayPayoutUrl;
	
	private String dragonpayBaseUrl;

	public String getDragonpayBaseUrl() {
		if(dragonpayTestMode.equalsIgnoreCase("True"))
			dragonpayBaseUrl = dragonpayTestUrl + dragonpaySoapUrl;
		else
			dragonpayBaseUrl = dragonpayProdUrl + dragonpaySoapUrl;
		return dragonpayBaseUrl;
	}

	public void setDragonpayBaseUrl(String dragonpayBaseUrl) {
		this.dragonpayBaseUrl = dragonpayBaseUrl;
	}
	

	
	@Value("${social.media.banner.dir}")
	private String socialMediaBannerDir;
	
	@Value("${email.attachment}")
	private String emailAttachment;
}
