package com.piep.piepapi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalancePayment;
import com.piep.piepapi.domain.MemberBalancePaymentHeader;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.exception.ApiError;
import com.piep.piepapi.service.MemberBalancePaymentService;
import com.piep.piepapi.service.MemberBalanceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "MemberBalancePayment", value = "MemberBalancePayment API", description = "Member Balance Payment API")
@RestController
@RequestMapping("/api/v1/payments/balance/payment")
public class MemberBalancePaymentController {

	private static Logger logger = LoggerFactory.getLogger(MemberBalancePaymentController.class);

	@Autowired
	MemberBalancePaymentService memberBalancePaymentService;

	@CrossOrigin
	@ApiOperation(value = "Get list of members balance payment")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@ApiImplicitParams({
		@ApiImplicitParam(name = "balance_id", dataType = "int", paramType = "query", value = "Balance ID", required = false),
		@ApiImplicitParam(name = "payment_id", dataType = "int", paramType = "query", value = "Payment ID", required = false),
		@ApiImplicitParam(name = "created_date", dataType = "date", paramType = "query", value = "Created Date", required = false),
		@ApiImplicitParam(name = "created_by", dataType = "string", paramType = "query", value = "Created By", required = false),
		@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Page to retrieve (0..N). Default value is 0.", required = false),
		@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Entries per page. Default value is 20.", required = false),
		@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sort criteria: property(,asc|desc). "
				+ "Multiple sort criteria supported. Default is not sorted.") })
	@GetMapping(path = "", produces = "application/json")
	public ResponseEntity<Page<MemberBalancePayment>> findAll(
			@RequestParam(name = "balance_id", required = false) Integer balanceId,
			@RequestParam(name = "payment_id", required = false) Integer paymentId,
			@RequestParam(name = "created_date", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy")  Date createdDate,
			@RequestParam(name = "created_by", required = false) String createdBy,
			Pageable pageable) throws Exception {
		logger.info("Get list of members balance payment");

		return new ResponseEntity<Page<MemberBalancePayment>>(
				memberBalancePaymentService.findAll(balanceId, paymentId, createdDate, createdBy, pageable),
				HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Create new Member balance payment")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Page<MemberBalancePaymentHeader>> create(@RequestBody(required = true) List<MemberBalancePaymentHeader> paymentBalance,Pageable pageable)
			throws Exception {
		logger.info("Create new Member balance payment");

		return new ResponseEntity<Page<MemberBalancePaymentHeader>>(memberBalancePaymentService.create(paymentBalance,pageable), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get Membership balance payment detail by ID")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<MemberBalancePayment> findById(@PathVariable(name = "id") int id) throws Exception {
		logger.info("Get Membership balance payment detail by ID");

		return new ResponseEntity<MemberBalancePayment>(memberBalancePaymentService.findById(id), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Update Member balance payment detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<MemberBalancePaymentHeader> update(@PathVariable(name = "id") int id,
			@RequestBody(required = true) MemberBalancePaymentHeader paymentBalance) throws Exception {

		logger.info("Update Member balance payment detail");
		paymentBalance.setId(id);
		return new ResponseEntity<MemberBalancePaymentHeader>(memberBalancePaymentService.update(paymentBalance), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Delete Member payment balance")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@DeleteMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<MemberBalancePayment> delete(@PathVariable(name = "id") int id) throws Exception {

		logger.info("Delete Member payment balance");

		return new ResponseEntity<MemberBalancePayment>(memberBalancePaymentService.delete(id), HttpStatus.OK);
	}
}
