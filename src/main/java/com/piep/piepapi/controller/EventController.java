package com.piep.piepapi.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.Event;
import com.piep.piepapi.email.model.SocialMediaBanner;
import com.piep.piepapi.exception.ApiError;
import com.piep.piepapi.service.EventService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Events", value = "Events API", description = "Events API")
@RestController
@RequestMapping("/api/v1/events")
public class EventController {
	private static Logger logger = LoggerFactory.getLogger(EventController.class);

	@Autowired
	EventService eventService;

	@CrossOrigin
	@ApiOperation(value = "Get the list of events, finished events not included")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Page<Event>> find(@RequestParam(name = "title", required = false) Long supplierId,
			@RequestParam(name = "buyerId", required = false) Long buyerId,
			@RequestParam(name = "referenceNumber", required = false) String referenceNumber,
			@RequestParam(name = "status", required = false) String status, Pageable pageable) throws Exception {
		logger.info("Get the list of events, finished events not included");

		Date endDate = new Date();
		return new ResponseEntity<Page<Event>>(eventService.find(endDate, pageable), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get the list of events created")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/all", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Page<Event>> findAll(Pageable pageable) throws Exception {
		logger.info("Get the list of events created");

		return new ResponseEntity<Page<Event>>(eventService.findAll(pageable), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Create new event")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/create", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Event> create(@RequestBody(required = true) Event event) throws Exception {
		logger.info("Create new event");

		return new ResponseEntity<Event>(eventService.create(event), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get event record base on specified event_id")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/{event_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Event> findById(@PathVariable(name = "event_id") int eventId) throws Exception {
		logger.info("Get event record base on specified event_id");

		return new ResponseEntity<Event>(eventService.findById(eventId), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Update event record base on specified event_id")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PatchMapping(path = "/{event_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Event> update(@PathVariable(name = "event_id") int eventId, @RequestBody(required = true) Event event) throws Exception {

		logger.info("Update event record base on specified event_id");

		return new ResponseEntity<Event>(eventService.update(eventId, event), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Delete event record base on specified event_id")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@DeleteMapping(path = "/{event_id}", produces = "application/json")
	public ResponseEntity<Event> delete(@PathVariable(name = "event_id") int eventId) throws Exception {

		logger.info("Delete event record base on specified event_id");

		return new ResponseEntity<Event>(eventService.delete(eventId), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Update event detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Event> update(@PathVariable(name = "id") int id,
			@RequestBody(required = true) SocialMediaBanner updates) throws Exception {

		logger.info("Update event detail");
		return new ResponseEntity<Event>(eventService.update(id, updates), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get event social media banner")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/banner/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Resource> download(@PathVariable(name = "id") int id) throws Exception {
		logger.info("Get event social media banner");

		Event event = eventService.findById(id);
		Resource resource = eventService.download(id);

		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/octet-stream"))
				.header(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"" + event.getSocialMediaBanner() + "\"")
				.body(resource);
	}

	@CrossOrigin
	@ApiOperation(value = "Get Past Events")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/past", produces = "application/json")
	public ResponseEntity<Page<Event>> findPastEvent(Pageable pageable) throws Exception {

		logger.info("Get Past Events");
		Date end = new Date();
		return new ResponseEntity<Page<Event>>(eventService.findPastEvent(end, pageable), HttpStatus.OK);
	}
}
