package com.piep.piepapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.Chapter;
import com.piep.piepapi.service.ChapterService;

//@RestController
//@RequestMapping("/api/v1/chapters")
public class ChapterController {

	private static Logger logger = LoggerFactory.getLogger(ChapterController.class);

	@Autowired
	ChapterService chapterService;

	@GetMapping(path = "", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Page<Chapter>> findAll(Pageable pageable) throws Exception {
		logger.info("Get list of Chapters");

		return new ResponseEntity<Page<Chapter>>(chapterService.findAll(pageable), HttpStatus.OK);
	}

	@PostMapping(path = "/create", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Chapter> create(@RequestBody(required = true) Chapter chapter) throws Exception {
		logger.info("Create new chapter");

		return new ResponseEntity<Chapter>(chapterService.create(chapter), HttpStatus.OK);
	}

	@GetMapping(path = "/{chapter_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Chapter> findById(@PathVariable(name = "chapter_id") int chapterId) throws Exception {
		logger.info("Get chapter based on Id");

		return new ResponseEntity<Chapter>(chapterService.findById(chapterId), HttpStatus.OK);
	}
 
	@PutMapping(path = "/{chapter_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Chapter> update(@PathVariable(name = "chapter_id") int chapterId,
			@RequestBody(required = true) Chapter chapter) throws Exception {

		logger.info("Update Chapter details");
		chapter.setId(chapterId);
		return new ResponseEntity<Chapter>(chapterService.update( chapter), HttpStatus.OK);
	}

	@DeleteMapping(path = "/{chapter_id}", produces = "application/json")
	public ResponseEntity<Chapter> delete(@PathVariable(name = "chapter_id") int chapterId) throws Exception {

		logger.info("Delete Chapter details");

		return new ResponseEntity<Chapter>(chapterService.delete(chapterId), HttpStatus.OK);
	}
}
