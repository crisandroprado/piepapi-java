package com.piep.piepapi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalanceHeader;
import com.piep.piepapi.domain.Membership;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.exception.ApiError;
import com.piep.piepapi.service.MemberBalanceService;
import com.piep.piepapi.service.MembershipService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/*@Api(tags = "Membership", value = "Membership API", description = "Membership API")
@RestController
@RequestMapping("/api/v1/payments/membership")*/
public class MembershipController {

	/*private static Logger logger = LoggerFactory.getLogger(MembershipController.class);

	@Autowired
	MembershipService membershipService;

	@CrossOrigin
	@ApiOperation(value = "Get list of membership payment")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@ApiImplicitParams({
			@ApiImplicitParam(name = "startYear", dataType = "int", paramType = "query", value = "startYear", required = false),
			@ApiImplicitParam(name = "endYear", dataType = "int", paramType = "query", value = "endYear", required = false),
			@ApiImplicitParam(name = "firstName", dataType = "String", paramType = "query", value = "firstName", required = false),
			@ApiImplicitParam(name = "lastName", dataType = "String", paramType = "query", value = "lastName", required = false),
			@ApiImplicitParam(name = "status", dataType = "String", paramType = "query", value = "status", required = false),
			@ApiImplicitParam(name = "startDate", dataType = "Date", paramType = "query", value = "startDate", required = false),
			@ApiImplicitParam(name = "endDate", dataType = "Date", paramType = "query", value = "endDate", required = false),
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Page to retrieve (0..N). Default value is 0.", required = false),
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Entries per page. Default value is 20.", required = false),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sort criteria: property(,asc|desc). "
					+ "Multiple sort criteria supported. Default is not sorted.") })
	@GetMapping(path = "", produces = "application/json")
	public ResponseEntity<Page<Membership>> findAll(
			@RequestParam(name = "startYear", required = false) Integer startYear,
			@RequestParam(name = "endYear", required = false) Integer endYear,
			@RequestParam(name = "firstName", required = false) String firstName,
			@RequestParam(name = "lastName", required = false) String lastName,
			@RequestParam(name = "status", required = false) String status,
			@RequestParam(name = "startDate", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy")  Date startDate,
			@RequestParam(name = "endDate", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy")  Date endDate,
			Pageable pageable) throws Exception {
		logger.info("Get list of membership payment");

		return new ResponseEntity<Page<Membership>>(membershipService.findAll(startYear, endYear, firstName, lastName, status, startDate, endDate, pageable), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Create bulk Membership")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/bulk", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Page<Membership>> create(@RequestBody(required = true) List<Membership> membership,Pageable pageable)
			throws Exception {
		logger.info("Create bulk Membership");

		return new ResponseEntity<Page<Membership>>(membershipService.createAll(membership,pageable), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get Membership detail by ID")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<Membership> findById(@PathVariable(name = "id") int id) throws Exception {
		logger.info("Get Membership balance detail by ID");

		return new ResponseEntity<Membership>(membershipService.findById(id), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Update Membership detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Membership> update(@PathVariable(name = "id") int id,
			@RequestBody(required = true) Membership membership) throws Exception {

		logger.info("Update Member balance detail");
		membership.setId(id);
		return new ResponseEntity<Membership>(membershipService.update(membership), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Delete Membership")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@DeleteMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<Membership> delete(@PathVariable(name = "id") int id) throws Exception {

		logger.info("Delete Member balance");

		return new ResponseEntity<Membership>(membershipService.delete(id), HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Create Membership")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Membership> create(@RequestBody(required = true) Membership membership)
			throws Exception {
		logger.info("Create Membership");

		return new ResponseEntity<Membership>(membershipService.create(membership), HttpStatus.OK);
	}*/
}
