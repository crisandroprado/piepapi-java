package com.piep.piepapi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalanceHeader;
import com.piep.piepapi.email.model.ExcelFile;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.email.model.MemberDues;
import com.piep.piepapi.exception.ApiError;
import com.piep.piepapi.service.MemberBalanceService;
import com.piep.piepapi.util.FileUtility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "MemberBalance", value = "MemberBalance API", description = "Member Balance API")
@RestController
@RequestMapping("/api/v1/payments/balance")
public class MemberBalanceController {

	private static Logger logger = LoggerFactory.getLogger(MemberBalanceController.class);

	@Autowired
	MemberBalanceService paymentBalanceService;

	@CrossOrigin
	@ApiOperation(value = "Get list of members balance")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@ApiImplicitParams({
			@ApiImplicitParam(name = "description", dataType = "String", paramType = "query", value = "Description", required = false),
			@ApiImplicitParam(name = "user_id", dataType = "int", paramType = "query", value = "User ID", required = false),
			@ApiImplicitParam(name = "chapted_id", dataType = "int", paramType = "query", value = "Chapter ID", required = false),
			@ApiImplicitParam(name = "is_paid", dataType = "int", paramType = "query", value = "Is Paid", required = false),
			@ApiImplicitParam(name = "is_membership", dataType = "int", paramType = "query", value = "Payment is for Membership", required = false),
			@ApiImplicitParam(name = "start_date", dataType = "date", paramType = "query", value = "Created Date Start", required = false),
			@ApiImplicitParam(name = "end_date", dataType = "date", paramType = "query", value = "Creatd Date End", required = false),
			@ApiImplicitParam(name = "start_amount", dataType = "double", paramType = "query", value = "Amount Start", required = false),
			@ApiImplicitParam(name = "end_amount", dataType = "double", paramType = "query", value = "Amount End", required = false),
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Page to retrieve (0..N). Default value is 0.", required = false),
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Entries per page. Default value is 20.", required = false),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sort criteria: property(,asc|desc). "
					+ "Multiple sort criteria supported. Default is not sorted.") })
	@GetMapping(path = "", produces = "application/json")
	public ResponseEntity<Page<MemberBalance>> findAll(
			@RequestParam(name = "description", required = false) String description,
			@RequestParam(name = "user_id", required = false) Integer userId,
			@RequestParam(name = "chapter_id", required = false) Integer chapterId,
			@RequestParam(name = "is_paid", required = false) Integer isPaid,
			@RequestParam(name = "is_membership", required = false) Integer isMembership,
			@RequestParam(name = "start_date", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy") Date startDate,
			@RequestParam(name = "end_date", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy") Date endDate,
			@RequestParam(name = "start_amount", required = false) Double startAmount,
			@RequestParam(name = "end_amount", required = false) Double endAmount, Pageable pageable) throws Exception {
		logger.info("Get list of members balance");

		return new ResponseEntity<Page<MemberBalance>>(paymentBalanceService.findAll(userId, chapterId, description,
				isPaid, isMembership, startDate, endDate, startAmount, endAmount, pageable), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Create new Member balance")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Page<MemberBalanceHeader>> create(
			@RequestBody(required = true) List<MemberBalanceHeader> paymentBalance, Pageable pageable)
			throws Exception {
		logger.info("Create new Member balance");

		return new ResponseEntity<Page<MemberBalanceHeader>>(paymentBalanceService.create(paymentBalance, pageable),
				HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get Membership balance detail by ID")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<MemberBalance> findById(@PathVariable(name = "id") int id) throws Exception {
		logger.info("Get Membership balance detail by ID");

		return new ResponseEntity<MemberBalance>(paymentBalanceService.findById(id), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Update Member balance detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<MemberBalanceHeader> update(@PathVariable(name = "id") int id,
			@RequestBody(required = true) MemberBalanceHeader paymentBalance) throws Exception {

		logger.info("Update Member balance detail");
		paymentBalance.setId(id);
		return new ResponseEntity<MemberBalanceHeader>(paymentBalanceService.update(paymentBalance), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Delete Member balance")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@DeleteMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<MemberBalanceHeader> delete(@PathVariable(name = "id") int id) throws Exception {

		logger.info("Delete Member balance");
		return new ResponseEntity<MemberBalanceHeader>(paymentBalanceService.delete(id), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get Membership balance detail by Payment Id")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/list/payment/{id}", produces = "application/json")
	public ResponseEntity<List<MemberBalance>> findByPaymentId(@PathVariable(name = "id") int paymentId)
			throws Exception {
		logger.info("Get Membership balance detail by ID");

		return new ResponseEntity<List<MemberBalance>>(
				paymentBalanceService.findByMemberBalancePaymentPaymentId(paymentId), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Send Manual Payment Email")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/email/payment/{id}", produces = "application/json")
	public ResponseEntity<GeneralResponse> sendMail(@PathVariable(name = "id") int paymentId) throws Exception {
		logger.info("Get Membership balance detail by ID");

		return new ResponseEntity<GeneralResponse>(paymentBalanceService.sendManualEmailPayment(paymentId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Upload Member Balance File")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/upload", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Page<MemberBalanceHeader>> create(@RequestBody(required = true) ExcelFile excelFile,
			Pageable pageable) throws Exception {
		logger.info("Upload Member Balance File");
		// try {
		FileUtility fileUtility = new FileUtility();
		List<MemberDues> memberDues = fileUtility.decodeBase64MemberDues(excelFile);
		logger.info("Create member dues");
		return new ResponseEntity<Page<MemberBalanceHeader>>(paymentBalanceService.upload(memberDues, pageable),
				HttpStatus.OK);
		// response.setResponse("OK");
		// response.setMessage("Upload Successfully");
		// }
		// catch(Exception ex) {
		// response.setResponse("Error");
		// response.setMessage(ex.toString());
		// }

	}
	
	@CrossOrigin
	@ApiOperation(value = "Update Member balance payment detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PatchMapping(path = "/payment/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<MemberBalanceHeader> updatePayment(@PathVariable(name = "id") int id) throws Exception {

		logger.info("Update Member balance detail");
		return new ResponseEntity<MemberBalanceHeader>(paymentBalanceService.update(id), HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Create Membership Balance")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/membership/{year}/{amount}", produces = "application/json")
	public ResponseEntity<Page<MemberBalanceHeader>> createMembershipBalance(@PathVariable(name = "year") String year,@PathVariable(name = "amount") double amount,
			Pageable pageable) throws Exception {
		logger.info("Create Membership Balance");
		return new ResponseEntity<Page<MemberBalanceHeader>>(paymentBalanceService.membership(year, amount, pageable),
				HttpStatus.OK);
	
	}
}
