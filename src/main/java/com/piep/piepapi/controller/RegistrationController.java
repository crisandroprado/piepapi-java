package com.piep.piepapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.Registration;
import com.piep.piepapi.service.RegistrationService;

//@RestController
//@RequestMapping("/api/v1/events/{event_id}/registrations")
public class RegistrationController {

	private static Logger logger = LoggerFactory.getLogger(RegistrationController.class);

	@Autowired
	RegistrationService registrationService;

	@GetMapping(path = "", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Page<Registration>> find(@PathVariable(name = "event_id") int eventId, Pageable pageable)
			throws Exception {
		logger.info("Get the list of Registered member under the given Event ID");

		return new ResponseEntity<Page<Registration>>(registrationService.findAll(eventId, pageable), HttpStatus.OK);
	}

	@PostMapping(path = "/create", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Registration> create(@PathVariable(name = "event_id") int eventId,
			@RequestBody(required = true) Registration registration) throws Exception {
		logger.info("Create Member event registration record");

		registration.setEventId(eventId);
		return new ResponseEntity<Registration>(registrationService.create(registration), HttpStatus.OK);
	}

	@GetMapping(path = "/{member_event_register_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Registration> findById(@PathVariable(name = "event_id") int eventId,
			@PathVariable(name = "member_event_register_id") int memberEventRegisterId) throws Exception {
		logger.info("Get the Member event registration");

		return new ResponseEntity<Registration>(registrationService.findById(eventId, memberEventRegisterId),
				HttpStatus.OK);
	}

	@PutMapping(path = "/{member_event_register_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Registration> update(@PathVariable(name = "event_id") int eventId,
			@PathVariable(name = "member_event_register_id") int memberEventRegisterId,
			@RequestBody(required = true) Registration registration) throws Exception {

		logger.info("Update the Member event registratio");

		registration.setId(memberEventRegisterId);
		registration.setEventId(eventId);
		return new ResponseEntity<Registration>(
				registrationService.update( registration), HttpStatus.OK);
	}

	@DeleteMapping(path = "/{member_event_register_id}", produces = "application/json")
	public ResponseEntity<Registration> delete(@PathVariable(name = "event_id") int eventId,
			@PathVariable(name = "member_event_register_id") int memberEventRegisterId) throws Exception {

		logger.info("Delete the Member event registration");

		return new ResponseEntity<Registration>(registrationService.delete(eventId, memberEventRegisterId),
				HttpStatus.OK);
	}
	

	@GetMapping(path = "/{member_event_register_id}/resend-ticket/", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Registration> resendTicket(@PathVariable(name = "event_id") int eventId,
			@PathVariable(name = "member_event_register_id") int memberEventRegisterId) throws Exception {
		logger.info("Re-send Event ticket to Member");

		return new ResponseEntity<Registration>(registrationService.resendTicket(eventId, memberEventRegisterId),
				HttpStatus.OK);
	}
}
