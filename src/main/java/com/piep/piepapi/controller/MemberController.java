package com.piep.piepapi.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.Member;
import com.piep.piepapi.domain.MemberBalanceHeader;
import com.piep.piepapi.domain.UserExport;
import com.piep.piepapi.email.model.ExcelFile;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.email.model.MemberData;
import com.piep.piepapi.exception.ApiError;
import com.piep.piepapi.service.MemberService;
import com.piep.piepapi.util.FileUtility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Member", value = "Member API", description = "Member API")
@RestController
@RequestMapping("/api/v1/members")
public class MemberController {

	private static Logger logger = LoggerFactory.getLogger(MemberController.class);

	@Autowired
	MemberService memberService;

	@CrossOrigin
	@ApiOperation(value = "Get list of members")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@ApiImplicitParams({
			@ApiImplicitParam(name = "chapter_id", dataType = "int", paramType = "query", value = "chapter_id", required = false),
			@ApiImplicitParam(name = "first_name", dataType = "string", paramType = "query", value = "first_name", required = false),
			@ApiImplicitParam(name = "last_name", dataType = "string", paramType = "query", value = "last_name", required = false),
			@ApiImplicitParam(name = "skills", dataType = "string", paramType = "query", value = "skills", required = false),
			@ApiImplicitParam(name = "industries", dataType = "string", paramType = "query", value = "industries", required = false),
			@ApiImplicitParam(name = "prc_license_number", dataType = "string", paramType = "query", value = "prc_license_number", required = false),
			@ApiImplicitParam(name = "year_obtained", dataType = "string", paramType = "query", value = "year_obtained", required = false),
			@ApiImplicitParam(name = "is_approved", dataType = "int", paramType = "query", value = "is_approved", required = false),
			@ApiImplicitParam(name = "middle_name", dataType = "string", paramType = "query", value = "middle_name", required = false),
			@ApiImplicitParam(name = "email", dataType = "string", paramType = "query", value = "email", required = false),
			@ApiImplicitParam(name = "start_date", dataType = "date", paramType = "query", value = "Created Date Start", required = false),
			@ApiImplicitParam(name = "end_date", dataType = "date", paramType = "query", value = "Creatd Date End", required = false),
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Page to retrieve (0..N). Default value is 0.", required = false),
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Entries per page. Default value is 20.", required = false),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sort criteria: property(,asc|desc). "
					+ "Multiple sort criteria supported. Default is not sorted.") })
	@GetMapping(path = "", produces = "application/json")
	public ResponseEntity<Page<Member>> findAll(@RequestParam(name = "chapter_id", required = false) Integer chapterId,
			@RequestParam(name = "first_name", required = false) String firstName,
			@RequestParam(name = "last_name", required = false) String lastName,
			@RequestParam(name = "skills", required = false) String skills,
			@RequestParam(name = "industries", required = false) String industries,
			@RequestParam(name = "prc_license_number", required = false) String prcLicenseNumber,
			@RequestParam(name = "year_obtained", required = false) String yearObtained,
			@RequestParam(name = "is_approved", required = false) Integer isApproved,
			@RequestParam(name = "middle_name", required = false) String middleName,
			@RequestParam(name = "email", required = false) String email,
			@RequestParam(name = "start_date", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy") Date startDate,
			@RequestParam(name = "end_date", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy") Date endDate,Pageable pageable)
			throws Exception {
		logger.info("Get list of members");

		return new ResponseEntity<Page<Member>>(memberService.findAll(chapterId, firstName, lastName, skills,
				industries, prcLicenseNumber, yearObtained,isApproved,middleName,email,startDate,endDate, pageable), HttpStatus.OK);
	}

	  @CrossOrigin
	  @ApiOperation(value = "Create new member")
	  @PostMapping(path = "", consumes = "application/json", produces = "application/json") 
	  public ResponseEntity<Member> create(@RequestBody(required= true) Member member) throws Exception { logger.info("Create new Member");
	     logger.info("Create new member");
	  	return new ResponseEntity<Member>(memberService.create(member), HttpStatus.OK);
	  }
	 

	@CrossOrigin
	@ApiOperation(value = "Get member by id")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/{member_id}", produces = "application/json")
	public ResponseEntity<Member> findById(@PathVariable(name = "member_id") int memberId) throws Exception {
		logger.info("Get member by id");

		return new ResponseEntity<Member>(memberService.findById(memberId), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Upload Member RFID Master File")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/rfid", consumes = "application/json", produces = "application/json")
	public ResponseEntity<GeneralResponse> create(@RequestBody(required = true) ExcelFile excelFile) throws Exception {

		GeneralResponse response = new GeneralResponse();
		try {
			FileUtility fileUtility = new FileUtility();
			List<MemberData> userRfid = fileUtility.decodeBase64UserData(excelFile);
			logger.info("Upload Member RFID Master File");
			memberService.update(userRfid);
			response.setResponse("OK");
			response.setMessage("Upload Successfully");
		} catch (Exception ex) {
			response.setResponse("Error");
			response.setMessage(ex.toString());
		}
		return new ResponseEntity<GeneralResponse>(response, HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Update Member rfid by id")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PutMapping(path = "/rfid/{member_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Member> update(@PathVariable(name = "member_id") int memberId,
			@RequestBody(required = true) MemberData userRfid) throws Exception {

		logger.info("Update Member rfid by id");
		return new ResponseEntity<Member>(memberService.update(memberId, userRfid.getRfid()), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "download list of members")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@ApiImplicitParams({
			@ApiImplicitParam(name = "chapter_id", dataType = "int", paramType = "query", value = "chapter_id", required = false),
			@ApiImplicitParam(name = "first_name", dataType = "string", paramType = "query", value = "first_name", required = false),
			@ApiImplicitParam(name = "last_name", dataType = "string", paramType = "query", value = "last_name", required = false),
			@ApiImplicitParam(name = "skills", dataType = "string", paramType = "query", value = "skills", required = false),
			@ApiImplicitParam(name = "industries", dataType = "string", paramType = "query", value = "industries", required = false),
			@ApiImplicitParam(name = "prc_license_number", dataType = "string", paramType = "query", value = "prc_license_number", required = false),
			@ApiImplicitParam(name = "year_obtained", dataType = "string", paramType = "query", value = "year_obtained", required = false),
			@ApiImplicitParam(name = "is_approved", dataType = "int", paramType = "query", value = "is_approved", required = false)})
	@GetMapping(path = "/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Resource> download(@RequestParam(name = "chapter_id", required = false) Integer chapterId,
			@RequestParam(name = "first_name", required = false) String firstName,
			@RequestParam(name = "last_name", required = false) String lastName,
			@RequestParam(name = "skills", required = false) String skills,
			@RequestParam(name = "industries", required = false) String industries,
			@RequestParam(name = "prc_license_number", required = false) String prcLicenseNumber,
			@RequestParam(name = "year_obtained", required = false) String yearObtained,
			@RequestParam(name = "is_approved", required = false) Integer isApproved)
			throws Exception {
		logger.info("download list of members");

		List<UserExport> userExports = memberService.download(chapterId, firstName, lastName, skills, industries, prcLicenseNumber, yearObtained,isApproved);
		FileUtility fileUtility = new FileUtility();

		Resource resource = fileUtility.download(userExports);
		String pattern = "MM-dd-yyyy";
		DateFormat df = new SimpleDateFormat(pattern);
		Date today = Calendar.getInstance().getTime();        
		String todayAsString = df.format(today);
		String filename =  "Members List-" + todayAsString  +  ".xlsx";
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType("application/octet-stream"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename  + "\"")
				.body(resource);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Update Member detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PutMapping(path = "/{member_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Member> update(@PathVariable(name = "member_id") int memberId,
			@RequestBody(required = true) Member member) throws Exception {

		logger.info("Update Member detail");
		member.setId(memberId);
		return new ResponseEntity<Member>(memberService.update(member), HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Upload Member Master File")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/masterfile", consumes = "application/json", produces = "application/json")
	public ResponseEntity<GeneralResponse> uploadMasterFile(@RequestBody(required = true) ExcelFile excelFile) throws Exception {

		GeneralResponse response = new GeneralResponse();
		try {
			FileUtility fileUtility = new FileUtility();
			List<MemberData> userRfid = fileUtility.decodeBase64MemberData(excelFile);
			logger.info("Upload Member Master File");
			memberService.update(userRfid);
			response.setResponse("OK");
			response.setMessage("Upload Successfully");
		} catch (Exception ex) {
			response.setResponse("Error");
			response.setMessage(ex.toString());
		}
		return new ResponseEntity<GeneralResponse>(response, HttpStatus.OK);
	}

	/*
	 * @DeleteMapping(path = "/{member_id}", produces = "application/json") public
	 * ResponseEntity<User> delete(@PathVariable(name = "member_id") int memberId)
	 * throws Exception {
	 * 
	 * logger.info("Delete Member");
	 * 
	 * return new ResponseEntity<User>(memberService.delete(memberId),
	 * HttpStatus.OK); }
	 */
}
