package com.piep.piepapi.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.UserAttendance;
import com.piep.piepapi.email.model.ExcelFile;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.exception.ApiError;
import com.piep.piepapi.service.UserAttendanceService;
import com.piep.piepapi.util.FileUtility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "UserAttendance", value = "UserAttendance API", description = "User Attendance API")
@RestController
@RequestMapping("/api/v1/user/attendance")
public class UserAttendanceController {

	private static Logger logger = LoggerFactory.getLogger(UserAttendanceController.class);

	@Autowired
	UserAttendanceService userAttendanceService;

	@CrossOrigin
	@ApiOperation(value = "Get list of user attendance")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@ApiImplicitParams({
			@ApiImplicitParam(name = "date_out", dataType = "String", paramType = "query", value = "Description", required = false),
			@ApiImplicitParam(name = "date_in", dataType = "String", paramType = "query", value = "User ID", required = false),
			@ApiImplicitParam(name = "updated_by", dataType = "string", paramType = "query", value = "Updated By", required = false),
			@ApiImplicitParam(name = "event_id", dataType = "int", paramType = "query", value = "Event ID", required = false),
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Page to retrieve (0..N). Default value is 0.", required = false),
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Entries per page. Default value is 20.", required = false),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sort criteria: property(,asc|desc). "
					+ "Multiple sort criteria supported. Default is not sorted.") })
	@GetMapping(path = "", produces = "application/json")
	public ResponseEntity<Page<UserAttendance>> findAll(
			@RequestParam(name = "date_out", required = false) String dateIn,
			@RequestParam(name = "date_in", required = false) String dateOut,
			@RequestParam(name = "event_id", required = false) Integer eventId,Pageable pageable) throws Exception {
		logger.info("Get list of user attendance");

		return new ResponseEntity<Page<UserAttendance>>(userAttendanceService.findAll(dateIn, dateOut,eventId, pageable),
				HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Upload Attendance File")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/upload/{event_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<GeneralResponse> create(@PathVariable(name = "event_id") int eventId, @RequestBody(required = true) ExcelFile excelFile) throws Exception {
		logger.info("Upload Attendance File");
		GeneralResponse response = new GeneralResponse() ;
		try {
			FileUtility fileUtility = new FileUtility();
			List<UserAttendance> userAttendance = fileUtility.decodeBase64AttendanceData(eventId,excelFile);
			logger.info("Create user attendance");
			userAttendanceService.create(userAttendance);
			response.setResponse("OK");
			response.setMessage("Upload Successfully");
		}
		catch(Exception ex) {
			response.setResponse("Error");
			response.setMessage(ex.toString());
		}
		return new ResponseEntity<GeneralResponse>(response, HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get Attendance By RFID")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/{rfid}", produces = "application/json")
	public ResponseEntity<Page<UserAttendance>> findById(@PathVariable(name = "rfid") String rfid,Pageable pageable) throws Exception {
		logger.info("Get Attendance By RFID");

		return new ResponseEntity<Page<UserAttendance>>(userAttendanceService.findByRfid(rfid,pageable), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Delete Attendance By RFID")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@DeleteMapping(path = "/{rfid}", produces = "application/json")
	public ResponseEntity<List<UserAttendance>> delete(@PathVariable(name = "rfid") String rfid) throws Exception {

		logger.info("Delete Attendance By RFID");

		return new ResponseEntity<List<UserAttendance>>(userAttendanceService.delete(rfid), HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Get Attendance By User ID")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "member/{id}", produces = "application/json")
	public ResponseEntity<Page<UserAttendance>> findByUserId(@PathVariable(name = "id") int id,Pageable pageable) throws Exception {
		logger.info("Get Attendance By User ID");

		return new ResponseEntity<Page<UserAttendance>>(userAttendanceService.findByUserId(id,pageable), HttpStatus.OK);
	}
}
