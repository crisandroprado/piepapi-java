package com.piep.piepapi.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.exception.ApiError;
import com.piep.piepapi.service.ImageResizerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Api(tags = "ImageResizer", value = "ImageResizer API", description = "ImageResizer API")
@RestController
@RequestMapping("/api/v1/image")
public class ImageController {
	private static Logger logger = LoggerFactory.getLogger(ImageController.class);
	
	@Autowired
	private ImageResizerService imageResizerService;

	@CrossOrigin
	@ApiOperation(value = "Resize Image")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(value = "/resize/file", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Resource> resizeImage(String imageUrl, Integer width, Integer height, Double percentage,
			HttpServletRequest request) throws Exception {
		logger.info("converting image={}, width={}, height={}, percentage={}", imageUrl, width, height, percentage);
		String format = "png";
		if (imageUrl.contains(".")) {
			format = imageUrl.substring(imageUrl.lastIndexOf(".") + 1);
		}

		String contentType = null;
		Resource resource = imageResizerService.resizeImage(imageUrl, format, width, height, percentage);

		try {
			contentType = request.getServletContext().getMimeType(imageUrl);
		} catch (Exception ex) {
			// expected
		}

		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType((contentType == null ? "application/octet-stream" : contentType)))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"response." + format + "\"")
				.body(resource);
	}
}
