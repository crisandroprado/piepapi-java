package com.piep.piepapi.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.MemberBalance;
import com.piep.piepapi.domain.MemberBalanceHeader;
import com.piep.piepapi.domain.Payment;
import com.piep.piepapi.service.MemberBalanceService;
import com.piep.piepapi.service.PaymentService;
import com.piep.piepapi.exception.ApiError;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Payment", value = "Payment API", description = "Payment API")
@RestController
@RequestMapping("/api/v1/payments")
public class PaymentController {

	private static Logger logger = LoggerFactory.getLogger(PaymentController.class);

	@Autowired
	PaymentService paymentService;

	@CrossOrigin
	@ApiOperation(value = "Get list of payments")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@ApiImplicitParams({
		@ApiImplicitParam(name = "user", dataType = "Integer", paramType = "query", value = "user", required = false),
		@ApiImplicitParam(name = "type", dataType = "int", paramType = "query", value = "type", required = false),
		@ApiImplicitParam(name = "transaction_number", dataType = "int", paramType = "query", value = "transaction_number", required = false),
		@ApiImplicitParam(name = "token", dataType = "date", paramType = "query", value = "token", required = false),
		@ApiImplicitParam(name = "reference_number", dataType = "date", paramType = "query", value = "reference_number", required = false),
		@ApiImplicitParam(name = "status", dataType = "string", paramType = "query", value = "status", required = false),
		@ApiImplicitParam(name = "method", dataType = "string", paramType = "query", value = "method", required = false),
		@ApiImplicitParam(name = "amount", dataType = "Double", paramType = "query", value = "amount", required = false),
		@ApiImplicitParam(name = "created_at", dataType = "Date", paramType = "query", value = "created_at", required = false),
		@ApiImplicitParam(name = "updated_at", dataType = "Date", paramType = "query", value = "updated_at", required = false),
		@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Page to retrieve (0..N). Default value is 0.", required = false),
		@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Entries per page. Default value is 20.", required = false),
		@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sort criteria: property(,asc|desc). "
				+ "Multiple sort criteria supported. Default is not sorted.") })
	@GetMapping(path = "", produces = "application/json")
	public ResponseEntity<Page<Payment>> findAll(
			@RequestParam(name = "user", required = false) Integer user,
			@RequestParam(name = "type", required = false) String type,
			@RequestParam(name = "transaction_number", required = false) String transaction_number,
			@RequestParam(name = "token", required = false) String token,
			@RequestParam(name = "reference_number", required = false) String reference_number,
			@RequestParam(name = "status", required = false) String status,
			@RequestParam(name = "method", required = false) String method,
			@RequestParam(name = "amount", required = false) Double amount, 
			@RequestParam(name = "created_at", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy")  Date created_at,
			@RequestParam(name = "updated_at", required = false) @DateTimeFormat(pattern = "MM-dd-yyyy")  Date updated_at, Pageable pageable) throws Exception {
		logger.info("Get list of payment");

		return new ResponseEntity<Page<Payment>>(
				paymentService.findAll(user, type, transaction_number, token, reference_number, status,method, amount, created_at, updated_at, pageable),
				HttpStatus.OK);
	}

	/*@ApiOperation(value = "Create new Member balance")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "", consumes = "application/json", produces = "application/json")
	public ResponseEntity<MemberBalanceHeader> create(@RequestBody(required = true) MemberBalanceHeader paymentBalance)
			throws Exception {
		logger.info("Create new Member balance");

		return new ResponseEntity<MemberBalanceHeader>(paymentBalanceService.create(paymentBalance), HttpStatus.OK);
	}*/

	@CrossOrigin
	@ApiOperation(value = "Get Payment detail by ID")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<Payment> findById(@PathVariable(name = "id") int id) throws Exception {
		logger.info("Get Membership balance detail by ID");

		return new ResponseEntity<Payment>(paymentService.findById(id), HttpStatus.OK);
	}

	/*@ApiOperation(value = "Update Member balance detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<MemberBalanceHeader> update(@PathVariable(name = "id") int id,
			@RequestBody(required = true) MemberBalanceHeader paymentBalance) throws Exception {

		logger.info("Update Member balance detail");
		paymentBalance.setId(id);
		return new ResponseEntity<MemberBalanceHeader>(paymentBalanceService.update(paymentBalance), HttpStatus.OK);
	}

	@ApiOperation(value = "Delete Member balance")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@DeleteMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<MemberBalance> delete(@PathVariable(name = "id") int id) throws Exception {

		logger.info("Delete Member balance");

		return new ResponseEntity<MemberBalance>(paymentBalanceService.delete(id), HttpStatus.OK);
	}*/
}
