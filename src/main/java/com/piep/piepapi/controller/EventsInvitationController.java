package com.piep.piepapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.piep.piepapi.domain.EventsInvitation;
import com.piep.piepapi.email.model.GeneralResponse;
import com.piep.piepapi.exception.ApiError;
import com.piep.piepapi.service.EventsInvitationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "EventsInvitation", value = "Events Invitation API", description = "Events Invitation API")
@RestController
@RequestMapping("/api/v1/events/invitation")
public class EventsInvitationController {

	private static Logger logger = LoggerFactory.getLogger(EventsInvitationController.class);

	@Autowired
	EventsInvitationService eventsInvitationService;

	@CrossOrigin
	@ApiOperation(value = "Get list of Events Invitation")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@ApiImplicitParams({
			@ApiImplicitParam(name = "event_id", dataType = "int", paramType = "query", value = "Events Id", required = false),
			@ApiImplicitParam(name = "user_id", dataType = "int", paramType = "query", value = "User ID", required = false),
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Page to retrieve (0..N). Default value is 0.", required = false),
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Entries per page. Default value is 20.", required = false),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sort criteria: property(,asc|desc). "
					+ "Multiple sort criteria supported. Default is not sorted.") })
	@GetMapping(path = "", produces = "application/json")
	public ResponseEntity<Page<EventsInvitation>> findAll(@RequestParam(name = "event_id", required = false) Integer eventId,
			@RequestParam(name = "user_id", required = false) Integer userId,Pageable pageable) throws Exception {
		logger.info("Get list of Events Invitation");

		return new ResponseEntity<Page<EventsInvitation>>(eventsInvitationService.findAll(eventId,userId,pageable), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Get Events Invitation by id")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@GetMapping(path = "/{invitation_id}", produces = "application/json")
	public ResponseEntity<EventsInvitation> findById(@PathVariable(name = "invitation_id") int invitationId)
			throws Exception {
		logger.info("Get Events Invitation by id");

		return new ResponseEntity<EventsInvitation>(eventsInvitationService.findById(invitationId), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Update Events Invitation detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PutMapping(path = "/{invitation_id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<EventsInvitation> update(@PathVariable(name = "invitation_id") int invitationId,
			@RequestBody(required = true) EventsInvitation eventsInvitation) throws Exception {

		logger.info("Update Events Invitation detail");
		eventsInvitation.setId(invitationId);
		return new ResponseEntity<EventsInvitation>(eventsInvitationService.update(eventsInvitation), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Delete Events Invitation detail")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@DeleteMapping(path = "/{invitation_id}", produces = "application/json")
	public ResponseEntity<EventsInvitation> delete(@PathVariable(name = "invitation_id") int invitationId)
			throws Exception {

		logger.info("Delete Events Invitation detail");
		return new ResponseEntity<EventsInvitation>(eventsInvitationService.delete(invitationId), HttpStatus.OK);
	}

	@CrossOrigin
	@ApiOperation(value = "Create new Events Invitation")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "", consumes = "application/json", produces = "application/json")
	public ResponseEntity<EventsInvitation> create(@RequestBody(required = true) EventsInvitation eventsInvitation)
			throws Exception {
		logger.info("Create new Events Invitation");
		return new ResponseEntity<EventsInvitation>(eventsInvitationService.create(eventsInvitation), HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Send Email for Events Invitation")
	@ApiResponses({ @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class) })
	@PostMapping(path = "/email/{invitation_id}", produces = "application/json")
	public ResponseEntity<GeneralResponse> send(@PathVariable(name = "invitation_id") int invitationId)
			throws Exception {
		logger.info("Send Email for Events Invitation");
		return new ResponseEntity<GeneralResponse>(eventsInvitationService.sendEventsInvitationEmail(invitationId), HttpStatus.OK);
	}

}
